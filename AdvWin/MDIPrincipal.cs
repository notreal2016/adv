﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdvWin.Forms;
using ADVCore.Context;
using ADVCore.Models;

namespace AdvWin
{
    public partial class MDIPrincipal : Form
    {

        private Splash _splash;
        private AdvContext _context;
        
        public MDIPrincipal(Splash splash)
        {
            InitializeComponent();
            _context = new AdvContext();
            _splash = splash;
        }

        public Splash Splash
        {
            get { return _splash; }
            set { _splash = value; }
        }

        public Usuario Usuario { get; set; }

        private void ShowNewForm(object sender, EventArgs e)
        {
            toolStripButton1_Click(sender, e);
        }

        private void OpenCidade(object sender, EventArgs e)
        {
            toolStripButton2_Click(sender, e);
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }
        

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form childForm = new Sobre();
            childForm.Text = "Sobre ADV";
            childForm.Show();
            childForm.Location = AutoScrollPosition;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Form chilForm = FrmCliente.GetInstance(_context);
            chilForm.MdiParent = this;
            chilForm.Show();
            chilForm.WindowState = FormWindowState.Normal;
            chilForm.WindowState =  FormWindowState.Maximized;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Form chilForm = new FrmCidade();
            chilForm.Show();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            Form chilForm = new FrmOperadora(_context);
            chilForm.Show();
        }

        private void MDIPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            _splash.Close();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {

            var resp = (Usuario.Tipo == TipoUser.ADMINISTRADOR)
                ? MessageBox.Show("Deseja abri seus dados para alteração?", this.Text, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information)
                : DialogResult.Yes;
            
            Form chilForm = (resp == DialogResult.No)? new FormUsuario():new FormUsuario(Usuario) ;
            chilForm.MdiParent = this;
            chilForm.Show();
            chilForm.WindowState = FormWindowState.Normal;
            chilForm.WindowState =  FormWindowState.Maximized;
        }

        private void operadoraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButton3_Click(sender, e);
        }

        private void MDIPrincipal_Load(object sender, EventArgs e)
        {
            labUser.Text = Usuario.Login;
        }

        private void usuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripButton4_Click(sender, e);
        }

        private void deslogarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _splash.Visible = true;
            _splash.Focus();
            this.Dispose();
        }

        private void escritoriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form chilForm = new FormEscritorio();
            chilForm.MdiParent = this;
            chilForm.Show();
            chilForm.WindowState = FormWindowState.Normal;
            chilForm.WindowState =  FormWindowState.Maximized;
        }

        private void btEscritorio_Click(object sender, EventArgs e)
        {
            escritoriosToolStripMenuItem_Click(sender, e);
        }

        private void MDIPrincipal_MouseDoubleClick(object sender, MouseEventArgs e)
        {
          
        }

        private void MDIPrincipal_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("www.silsoftwares.com");
        }
    }
}
