﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvWin
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Splash());
            }
            catch (Exception e)
            {
                if (e.HResult == -2146233036)
                {
                    MessageBox.Show(
                        "Erro de acesso a nuvem, contate o suporte 083 9 9107 6105.",
                        "Erro ao inicializar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(
                        "Erro de comunicação com o servidor de dados nas nuvens, verifique sua conexão, persistindo o problema contate o suporte 083 9 9107 6105.",
                        "Erro ao inicializar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
                
            }
           
        }
    }
}
