﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdvWin.Core;
using AdvWin.Forms;
using ADVCore.Context;
using ADVCore.Models;
using ADVCore.Service;
using Syncfusion.Linq;

namespace AdvWin
{
    public partial class FrmCliente : Form
    {
        private static FrmCliente _instance;
        private AdvContext _context;
        private static bool segue = false;
        private ADVCore.Service.ClienteService service;
        private CidadeService _cs;
        private long id = 0 ;
        private Usuario _user;
        private FrmCliente(AdvContext context)
        {
            InitializeComponent();
            _context = context;
            _cs = new CidadeService();
            service = new ClienteService();
            
            
        }

        public Usuario Usuario => _user;
        
        public static FrmCliente GetInstance(AdvContext context)
        {
            if (_instance == null)
            {
                _instance = new FrmCliente(context);
            }

            return _instance;
        }

        public void CriaListaClientes(long id)
        {
            try
            {
                var dados = service.FindClientes(this.id).ToArray();
                LBDados.DataSource = id > 0 ? dados.Where(c => c.IdEscritorio == id).ToArray(): dados ;
                LabRegistros.Text = String.Format("Total de cadastros:{0}",LBDados.Items.Count);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        

       

        private void FrmCliente_SizeChanged(object sender, EventArgs e)
        {
            LBDados.Width = this.Width;
            LBDados.Height = this.Height;
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            if (txtTermo.Text.Trim().Equals(""))
            {
                MessageBox.Show("Precisa de que seja digitado alguma informação para busca", "Erro de busca",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                txtTermo.Focus();
                return;
            }

            Busca(txtTermo.Text, (EnumTipoBusca) CobTipo.SelectedItem);

        }

        private void Busca(string termo, EnumTipoBusca filtro)
        {
            ICollection<InformeCliente> clientes = new List<InformeCliente>();
            var dados = service.FindClientes();
            try
            {
                switch (filtro)
                {
                    case EnumTipoBusca.CPF:
                        clientes = dados.Where(c => c.Cpf == termo).ToList();
                        break;
                    case EnumTipoBusca.PROTOCOLO:
                        clientes = dados.Where(c => c.Processo == termo).ToList();
                        break;
                    case EnumTipoBusca.NOME:
                        clientes = dados.Where(c => c.Nome.ToUpper().Contains(termo.ToUpper())).ToList();
                        break;
                    case EnumTipoBusca.CIDADE:
                        clientes = dados.Where(c => c.Cidade.ToUpper() == termo.ToUpper()).ToList();
                        break;
                    default:
                        clientes = dados;
                        break;
                }
                LBDados.DataSource = clientes.ToArray();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro na busca", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

        }

        private void BtDesfazer_Click(object sender, EventArgs e)
        {
            LBDados.DataSource = service.FindClientes().ToArray();
        }

        private void BtNovo_Click(object sender, EventArgs e)
        {
            Form chilForm = new FormFicha(this);
            chilForm.MdiParent = this.MdiParent;
            chilForm.Show();
            chilForm.WindowState = FormWindowState.Normal;
            chilForm.WindowState = FormWindowState.Maximized;

        }

        private void BtEditar_Click(object sender, EventArgs e)
        {
            if (LBDados.SelectedItem == null)
            {
                MessageBox.Show("Precisa antes selecionar um cliente na lista abaixo.", "Editando cliente",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
                
            }

            Cliente cliente = service.FindById( ((InformeCliente) LBDados.SelectedItem).Id);
            cliente = service.RefreshCliente(cliente);
            Form chilForm = new FormFicha(this, cliente);
            chilForm.MdiParent = this.MdiParent;
            chilForm.Text = LBDados.SelectedItem.ToString();
            chilForm.Show();
            chilForm.WindowState = FormWindowState.Normal;
            chilForm.WindowState = FormWindowState.Maximized;
        

        }

       

        private void LBDados_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            BtEditar_Click(sender, e);
        }

        private void FrmCliente_FormClosing(object sender, FormClosingEventArgs e)
        {
            _instance = null;
        }
        

        private void fichaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LBDados.SelectedItem != null)
            {
                
                Cliente c = service.FindById(((InformeCliente)LBDados.SelectedItem).Id);
                Form report = new FormReport("AdvWin.Report.FichaCliente.rdlc",
                    true, service.FichaCliente(c));
                report.MdiParent = this.MdiParent;
                report.Show();
                report.WindowState = FormWindowState.Normal;
                report.WindowState = FormWindowState.Maximized;
            }
        }

        private void filtroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form report = new FormReport("AdvWin.Report.RtpFiltro.rdlc",
                true, service.RelatorioFiltro(LBDados.Items.Cast<InformeCliente>().ToList()));
            report.MdiParent = this.MdiParent;
            report.Show();
            report.WindowState = FormWindowState.Normal;
            report.WindowState = FormWindowState.Maximized;
        }

        private void aniversariantesDoMêsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Form report = new FormReport("AdvWin.Report.RtpAniversariantes.rdlc",
                true, service.Aniversariantes(DateTime.Now.Month));
            report.MdiParent = this.MdiParent;
            report.Show();
            report.WindowState = FormWindowState.Normal;
            report.WindowState = FormWindowState.Maximized;
        }

        private void btRefresh_Click(object sender, EventArgs e)
        {
            var resposta =
                MessageBox.Show(
                    "O processo de sincronização pode demorar um pouco depedendo da sua internet, deseja continuar?",
                    Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (resposta == DialogResult.No)
            {
                return;
            }

            //segue = true;
            //Thread t= new  Thread(Contar);
            //t.Start();
            //service.RefreshAll();
            CriaListaClientes(((Escritorio) cobEscritorio.SelectedItem).Id);
            //segue = false;
            MessageBox.Show(
                "Concluido!!!",
                Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            
        }
        /*
        private static void Contar()
        {
            
                tBarra = new ToolStripProgressBar();
                while (segue)
                {
                    if (tBarra.Value >= 100)
                    {
                        tBarra.Value = 0;
                    }
                    tBarra.Value++;
                }
            
    }*/

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            CobTipo.Items.Add(EnumTipoBusca.NOME);
            CobTipo.Items.Add(EnumTipoBusca.CPF);
            CobTipo.Items.Add(EnumTipoBusca.PROTOCOLO);
            CobTipo.Items.Add(EnumTipoBusca.CIDADE);
            _user = ((MDIPrincipal) MdiParent).Usuario;
            var dados = _cs.Escritorios();
            Escritorio escritorio = null;
            cobEscritorio.Items.Clear();
            dados.ForEach(es =>
            {
                cobEscritorio.Items.Add(es);
                if (es.Id == _user.IdEscritorio)
                {
                    escritorio = es;
                }
            });
            cobEscritorio.SelectedItem = escritorio;
            CriaListaClientes(Usuario.Tipo == TipoUser.ADMINISTRADOR || escritorio == null?0:escritorio.Id);
        }

        private void cooperadorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form report = new FormReport("AdvWin.Report.RtpFiltro.rdlc",
                true, service.Colaboradores(LBDados.Items.Cast<InformeCliente>().ToList()));
            report.MdiParent = this.MdiParent;
            report.Show();
            report.WindowState = FormWindowState.Normal;
            report.WindowState = FormWindowState.Maximized;
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (cobEscritorio.SelectedItem == null)
            {
                MessageBox.Show("Para aplicar o filtro de escritório, deve ser seleionado um escritório", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            CriaListaClientes(((Escritorio) cobEscritorio.SelectedItem).Id);
        }

        private void btTodos_Click(object sender, EventArgs e)
        {
            CriaListaClientes(0);
        }
    }
    
}
