﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ADVCore.Context;
using ADVCore.Models.Concret;
using ADVCore.Service;

namespace AdvWin
{
    public partial class Splash : Form
    {
        private static MDIPrincipal mdi;
        private static bool iniciar = false, erro = false;
        private static UsuarioService _us;
        private static AdvContext _context = new AdvContext();
        public Splash()
        {
            InitializeComponent();
            try
            {
                _us = new UsuarioService(new UsuarioRepository());
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    "Erro de comunicação com o servidor de dados nas nuvens, verifique sua conexão, persistindo o problema contate o suporte 083 9 9107 6105.",
                    "Erro ao inicializar", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            /*Thread t = new Thread(NovaThread);
            t.Start();*/

            
        }

        private static void NovaThread()
        {
            try
            {
                ClienteService service = new ClienteService();
                iniciar = true;
            }
            catch (Exception e)
            {
                erro = true;
                MessageBox.Show("Erro de conexão, verifique sua internet e por favor tente novamente", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            /*if (this.barra.Value == 100)
            {
                barra.Value = 0;
            }

            barra.Value++;
            if (iniciar)
            {
                mdi = new MDIPrincipal(this);
                mdi.Show();
                this.Visible = false;
                timer1.Enabled = false;
            }

            if (erro)
            {
                barra.Value = 0;
                btNovamente.Visible = true;
                timer1.Enabled = false;
                
            }*/
        }

        private void btNovamente_Click(object sender, EventArgs e)
        {
            /*btNovamente.Visible = false;
            erro = false;
            timer1.Enabled = true;
            Thread t = new Thread(NovaThread);
            t.Start();*/
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {

            if (txtLogin.Text.Trim().Equals("") || txtSenha.Text.Trim().Equals(""))
            {
                MessageBox.Show("Login ou senha em brancos.", this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            if (ValidaDados.ExistCaracterEspeciais(txtLogin.Text))
            {
                MessageBox.Show("Login ou senha inválidos!", this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            try
            {
                var usuario = _us.Logar(txtLogin.Text, txtSenha.Text);
                if (usuario == null)
                {
                    MessageBox.Show("Login ou senha inválidos!", this.Text, MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                    return;
                }
                
                mdi = new MDIPrincipal(this);
                mdi.Usuario = usuario;
                mdi.Splash = this;
                mdi.Show();
                
                this.Visible = false;
            }
            catch (Exception exception)
            {
                String[] values = exception.Message.Split('-');
                if (values[0].Contains("50"))
                {
                    MessageBox.Show("Usuário ou senha inválidos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("Erro de conexão, verifique sua internet e por favor tente novamente", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                
            }
        }

        private void Splash_Load(object sender, EventArgs e)
        {

        }

        public static AdvContext Context
        {
            get { return _context; }
            set { _context = value; }
        }
    }
}
