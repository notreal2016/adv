﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ADVCore.Context;
using ADVCore.Models;
using ADVCore.Service;

namespace AdvWin.Forms
{
    public partial class FrmCidade : Form

    {
        private AdvContext _context;
        private CidadeService service;
        private ComboBox _combo;
        private Cidade _selecionada;
        public FrmCidade()
        {
            
            montaForm();
        }

        public FrmCidade(ComboBox combo)
        {
            _combo = combo;
            montaForm();
        }

        private void montaForm()
        {
            InitializeComponent();
            
            service = new CidadeService();
            var lista = service.Cidades();
            var escritorios = service.Escritorios();
            LbCidades.DataSource = (lista == null)?null:lista.ToList();
            cobEscritorio.DataSource = escritorios;
            cobEscritorio.SelectedItem = null;

        }
        private void btAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ValidaDados.ExistCaracterEspeciais(txtCidade.Text);
                ValidaDados.ExistCaracterEspeciais(txtUF.Text);
                if (txtCidade.Text.Trim().Equals(""))
                {
                    MessageBox.Show("Nome da cidade não pode ser em branco", "Erro cidade", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    txtCidade.Focus();
                    return;
                }
                if (txtUF.Text.Trim().Equals("") || txtUF.Text.Length != 2)
                {
                    MessageBox.Show("UF inválido", "Erro cidade", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    txtUF.Focus();
                    return;
                }

                if (cobEscritorio.SelectedItem == null)
                {
                    MessageBox.Show("Um escritório responsável deve ser selecionado.", "Erro cidade", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    cobEscritorio.Focus();
                    return;
                }
                if (_selecionada == null)
                {
                    _selecionada = new Cidade(txtCidade.Text.ToUpper(), txtUF.Text.ToUpper());
                    
                }
                else
                {
                    _selecionada.Uf = txtUF.Text;
                    _selecionada.Nome = txtCidade.Text;
                    
                }
                _selecionada.IdEscritorio = ((Escritorio) cobEscritorio.SelectedItem).Id;
                
                service.Save(_selecionada);
                LimpaCampos();
                LbCidades.DataSource = service.Cidades();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Erro cidade", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

            }
        }

        private void LimpaCampos()
        {
            _selecionada = null;
            txtUF.Text = "";
            txtCidade.Text = "";
            LbCidades.DataSource = service.Cidades().ToList();
            cobEscritorio.SelectedItem = null;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (_combo != null)
            {
                Cidade selecionada = (Cidade) _combo.SelectedItem;
                _combo.DataSource = null;
                _combo.DataSource = service.Cidades().ToList();
                _combo.Refresh();
                _combo.SelectedItem = selecionada;
            }
            Close();
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            if (LbCidades.SelectedItem == null)
            {
                MessageBox.Show("Precisa selecionar uma cidade para poder editar os dados.", "Seleção de cidade.",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            _selecionada = (Cidade) LbCidades.SelectedItem;
            txtUF.Text = _selecionada.Uf;
            txtCidade.Text = _selecionada.Nome;
            List<Escritorio> escritorios = (List<Escritorio>) cobEscritorio.DataSource;
            if ( _selecionada.IdEscritorio > 0 )
            {
                cobEscritorio.SelectedItem = escritorios.Single(es => es.Id == _selecionada.IdEscritorio);
            }
        }
    }
}
