﻿namespace AdvWin.Forms
{
    partial class FormFicha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFicha));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.MaskCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.CobCidade = new System.Windows.Forms.ComboBox();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.txtProcesso = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.RtRelatorio = new System.Windows.Forms.RichTextBox();
            this.picImagem = new System.Windows.Forms.PictureBox();
            this.DtAnimersario = new System.Windows.Forms.DateTimePicker();
            this.btSalvar = new System.Windows.Forms.Button();
            this.btCancelar = new System.Windows.Forms.Button();
            this.btCidade = new System.Windows.Forms.Button();
            this.btCaptura = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cobOperadora = new System.Windows.Forms.ComboBox();
            this.labOperadora = new System.Windows.Forms.Label();
            this.chWhatsapp = new System.Windows.Forms.CheckBox();
            this.btOperadora = new System.Windows.Forms.Button();
            this.txtDDD = new System.Windows.Forms.MaskedTextBox();
            this.txtTNumero = new System.Windows.Forms.MaskedTextBox();
            this.LbTelefones = new System.Windows.Forms.ListBox();
            this.btETelefone = new System.Windows.Forms.Button();
            this.btMTelefone = new System.Windows.Forms.Button();
            this.btTMenos = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cobTipo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picImagem)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Endereço:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(344, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nº.:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(390, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Bairro:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(541, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Cidade:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(505, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "CPF:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Ponto de referência:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 159);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Data de Nascimento:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(195, 159);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Nº. do Processo:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(417, 159);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "E-mail:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 391);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Relato:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(439, 199);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Foto:";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(16, 58);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(477, 20);
            this.txtNome.TabIndex = 1;
            // 
            // MaskCPF
            // 
            this.MaskCPF.Location = new System.Drawing.Point(508, 58);
            this.MaskCPF.Mask = "999,999,999-90";
            this.MaskCPF.Name = "MaskCPF";
            this.MaskCPF.Size = new System.Drawing.Size(252, 20);
            this.MaskCPF.TabIndex = 2;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(16, 99);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(324, 20);
            this.txtEndereco.TabIndex = 3;
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(346, 99);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(33, 20);
            this.txtNumero.TabIndex = 4;
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(391, 99);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(139, 20);
            this.txtBairro.TabIndex = 5;
            // 
            // CobCidade
            // 
            this.CobCidade.FormattingEnabled = true;
            this.CobCidade.Location = new System.Drawing.Point(544, 99);
            this.CobCidade.Name = "CobCidade";
            this.CobCidade.Size = new System.Drawing.Size(167, 21);
            this.CobCidade.TabIndex = 6;
            // 
            // txtReferencia
            // 
            this.txtReferencia.Location = new System.Drawing.Point(16, 136);
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.Size = new System.Drawing.Size(744, 20);
            this.txtReferencia.TabIndex = 8;
            // 
            // txtProcesso
            // 
            this.txtProcesso.Location = new System.Drawing.Point(198, 175);
            this.txtProcesso.Name = "txtProcesso";
            this.txtProcesso.Size = new System.Drawing.Size(216, 20);
            this.txtProcesso.TabIndex = 10;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(420, 175);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(340, 20);
            this.txtEmail.TabIndex = 11;
            // 
            // RtRelatorio
            // 
            this.RtRelatorio.Location = new System.Drawing.Point(16, 407);
            this.RtRelatorio.Name = "RtRelatorio";
            this.RtRelatorio.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.RtRelatorio.Size = new System.Drawing.Size(743, 142);
            this.RtRelatorio.TabIndex = 39;
            this.RtRelatorio.Text = "";
            // 
            // picImagem
            // 
            this.picImagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImagem.ErrorImage = ((System.Drawing.Image)(resources.GetObject("picImagem.ErrorImage")));
            this.picImagem.InitialImage = ((System.Drawing.Image)(resources.GetObject("picImagem.InitialImage")));
            this.picImagem.Location = new System.Drawing.Point(476, 201);
            this.picImagem.Name = "picImagem";
            this.picImagem.Size = new System.Drawing.Size(284, 186);
            this.picImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picImagem.TabIndex = 29;
            this.picImagem.TabStop = false;
            // 
            // DtAnimersario
            // 
            this.DtAnimersario.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtAnimersario.Location = new System.Drawing.Point(17, 175);
            this.DtAnimersario.Name = "DtAnimersario";
            this.DtAnimersario.Size = new System.Drawing.Size(175, 20);
            this.DtAnimersario.TabIndex = 9;
            // 
            // btSalvar
            // 
            this.btSalvar.Image = ((System.Drawing.Image)(resources.GetObject("btSalvar.Image")));
            this.btSalvar.Location = new System.Drawing.Point(712, 555);
            this.btSalvar.Name = "btSalvar";
            this.btSalvar.Size = new System.Drawing.Size(48, 44);
            this.btSalvar.TabIndex = 27;
            this.btSalvar.UseVisualStyleBackColor = true;
            this.btSalvar.Click += new System.EventHandler(this.btSalvar_Click);
            // 
            // btCancelar
            // 
            this.btCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btCancelar.Image")));
            this.btCancelar.Location = new System.Drawing.Point(17, 555);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(49, 44);
            this.btCancelar.TabIndex = 28;
            this.btCancelar.UseVisualStyleBackColor = true;
            this.btCancelar.Click += new System.EventHandler(this.button2_Click);
            // 
            // btCidade
            // 
            this.btCidade.Image = ((System.Drawing.Image)(resources.GetObject("btCidade.Image")));
            this.btCidade.Location = new System.Drawing.Point(717, 93);
            this.btCidade.Name = "btCidade";
            this.btCidade.Size = new System.Drawing.Size(43, 31);
            this.btCidade.TabIndex = 7;
            this.btCidade.TabStop = false;
            this.btCidade.UseVisualStyleBackColor = true;
            this.btCidade.Click += new System.EventHandler(this.btCidade_Click);
            // 
            // btCaptura
            // 
            this.btCaptura.Image = ((System.Drawing.Image)(resources.GetObject("btCaptura.Image")));
            this.btCaptura.Location = new System.Drawing.Point(434, 215);
            this.btCaptura.Name = "btCaptura";
            this.btCaptura.Size = new System.Drawing.Size(36, 30);
            this.btCaptura.TabIndex = 17;
            this.btCaptura.TabStop = false;
            this.btCaptura.UseVisualStyleBackColor = true;
            this.btCaptura.Click += new System.EventHandler(this.btCaptura_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "DDD:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(57, 240);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 34;
            this.label15.Text = "Número";
            // 
            // cobOperadora
            // 
            this.cobOperadora.FormattingEnabled = true;
            this.cobOperadora.Location = new System.Drawing.Point(205, 256);
            this.cobOperadora.Name = "cobOperadora";
            this.cobOperadora.Size = new System.Drawing.Size(162, 21);
            this.cobOperadora.TabIndex = 20;
            // 
            // labOperadora
            // 
            this.labOperadora.AutoSize = true;
            this.labOperadora.Location = new System.Drawing.Point(202, 240);
            this.labOperadora.Name = "labOperadora";
            this.labOperadora.Size = new System.Drawing.Size(60, 13);
            this.labOperadora.TabIndex = 36;
            this.labOperadora.Text = "Operadora:";
            // 
            // chWhatsapp
            // 
            this.chWhatsapp.AutoSize = true;
            this.chWhatsapp.Location = new System.Drawing.Point(374, 258);
            this.chWhatsapp.Name = "chWhatsapp";
            this.chWhatsapp.Size = new System.Drawing.Size(75, 17);
            this.chWhatsapp.TabIndex = 22;
            this.chWhatsapp.Text = "Whatsapp";
            this.chWhatsapp.UseVisualStyleBackColor = true;
            // 
            // btOperadora
            // 
            this.btOperadora.Image = ((System.Drawing.Image)(resources.GetObject("btOperadora.Image")));
            this.btOperadora.Location = new System.Drawing.Point(138, 357);
            this.btOperadora.Name = "btOperadora";
            this.btOperadora.Size = new System.Drawing.Size(35, 31);
            this.btOperadora.TabIndex = 21;
            this.btOperadora.TabStop = false;
            this.btOperadora.UseVisualStyleBackColor = true;
            this.btOperadora.Click += new System.EventHandler(this.btOperadora_Click);
            // 
            // txtDDD
            // 
            this.txtDDD.Location = new System.Drawing.Point(18, 257);
            this.txtDDD.Mask = "(09)";
            this.txtDDD.Name = "txtDDD";
            this.txtDDD.Size = new System.Drawing.Size(36, 20);
            this.txtDDD.TabIndex = 18;
            // 
            // txtTNumero
            // 
            this.txtTNumero.Location = new System.Drawing.Point(60, 257);
            this.txtTNumero.Name = "txtTNumero";
            this.txtTNumero.Size = new System.Drawing.Size(139, 20);
            this.txtTNumero.TabIndex = 19;
            // 
            // LbTelefones
            // 
            this.LbTelefones.FormattingEnabled = true;
            this.LbTelefones.Location = new System.Drawing.Point(18, 282);
            this.LbTelefones.Name = "LbTelefones";
            this.LbTelefones.Size = new System.Drawing.Size(452, 69);
            this.LbTelefones.TabIndex = 23;
            this.LbTelefones.TabStop = false;
            // 
            // btETelefone
            // 
            this.btETelefone.Image = ((System.Drawing.Image)(resources.GetObject("btETelefone.Image")));
            this.btETelefone.Location = new System.Drawing.Point(56, 357);
            this.btETelefone.Name = "btETelefone";
            this.btETelefone.Size = new System.Drawing.Size(36, 31);
            this.btETelefone.TabIndex = 26;
            this.btETelefone.TabStop = false;
            this.btETelefone.UseVisualStyleBackColor = true;
            this.btETelefone.Click += new System.EventHandler(this.btETelefone_Click);
            // 
            // btMTelefone
            // 
            this.btMTelefone.Image = ((System.Drawing.Image)(resources.GetObject("btMTelefone.Image")));
            this.btMTelefone.Location = new System.Drawing.Point(18, 357);
            this.btMTelefone.Name = "btMTelefone";
            this.btMTelefone.Size = new System.Drawing.Size(36, 31);
            this.btMTelefone.TabIndex = 25;
            this.btMTelefone.UseVisualStyleBackColor = true;
            this.btMTelefone.Click += new System.EventHandler(this.btMTelefone_Click);
            // 
            // btTMenos
            // 
            this.btTMenos.Image = ((System.Drawing.Image)(resources.GetObject("btTMenos.Image")));
            this.btTMenos.Location = new System.Drawing.Point(96, 357);
            this.btTMenos.Name = "btTMenos";
            this.btTMenos.Size = new System.Drawing.Size(36, 31);
            this.btTMenos.TabIndex = 38;
            this.btTMenos.TabStop = false;
            this.btTMenos.UseVisualStyleBackColor = true;
            this.btTMenos.Click += new System.EventHandler(this.btTMenos_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(26, 26);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(990, 33);
            this.toolStrip1.TabIndex = 39;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(30, 30);
            this.toolStripButton1.Text = "Imprimir Ficha";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 33);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Margin = new System.Windows.Forms.Padding(700, 1, 0, 2);
            this.toolStripButton2.MergeIndex = 40;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(30, 30);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.ToolTipText = "Excluir ficha";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(18, 217);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(210, 20);
            this.textBox1.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 201);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "Apelido:";
            // 
            // cobTipo
            // 
            this.cobTipo.FormattingEnabled = true;
            this.cobTipo.Location = new System.Drawing.Point(234, 215);
            this.cobTipo.Name = "cobTipo";
            this.cobTipo.Size = new System.Drawing.Size(193, 21);
            this.cobTipo.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(231, 198);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 13);
            this.label16.TabIndex = 41;
            this.label16.Text = "Tipo Cliente:";
            // 
            // FormFicha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancelar;
            this.ClientSize = new System.Drawing.Size(990, 604);
            this.Controls.Add(this.cobTipo);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.btTMenos);
            this.Controls.Add(this.btETelefone);
            this.Controls.Add(this.btMTelefone);
            this.Controls.Add(this.LbTelefones);
            this.Controls.Add(this.txtTNumero);
            this.Controls.Add(this.txtDDD);
            this.Controls.Add(this.btOperadora);
            this.Controls.Add(this.chWhatsapp);
            this.Controls.Add(this.cobOperadora);
            this.Controls.Add(this.labOperadora);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btCaptura);
            this.Controls.Add(this.btCidade);
            this.Controls.Add(this.btCancelar);
            this.Controls.Add(this.btSalvar);
            this.Controls.Add(this.DtAnimersario);
            this.Controls.Add(this.picImagem);
            this.Controls.Add(this.RtRelatorio);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtProcesso);
            this.Controls.Add(this.txtReferencia);
            this.Controls.Add(this.CobCidade);
            this.Controls.Add(this.txtBairro);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.txtEndereco);
            this.Controls.Add(this.MaskCPF);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormFicha";
            this.Text = "Ficha de cliente";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormFicha_FormClosing);
            this.Load += new System.EventHandler(this.FormFicha_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picImagem)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.MaskedTextBox MaskCPF;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.ComboBox CobCidade;
        private System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.TextBox txtProcesso;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.RichTextBox RtRelatorio;
        private System.Windows.Forms.PictureBox picImagem;
        private System.Windows.Forms.DateTimePicker DtAnimersario;
        private System.Windows.Forms.Button btSalvar;
        private System.Windows.Forms.Button btCancelar;
        private System.Windows.Forms.Button btCidade;
        private System.Windows.Forms.Button btCaptura;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cobOperadora;
        private System.Windows.Forms.Label labOperadora;
        private System.Windows.Forms.CheckBox chWhatsapp;
        private System.Windows.Forms.Button btOperadora;
        private System.Windows.Forms.MaskedTextBox txtDDD;
        private System.Windows.Forms.MaskedTextBox txtTNumero;
        private System.Windows.Forms.ListBox LbTelefones;
        private System.Windows.Forms.Button btETelefone;
        private System.Windows.Forms.Button btMTelefone;
        private System.Windows.Forms.Button btTMenos;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cobTipo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}