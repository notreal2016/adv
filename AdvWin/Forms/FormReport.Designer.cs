﻿namespace AdvWin.Forms
{
    partial class FormReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReport));
            this.rpview = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ClienteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.EnderecoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CidadeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ClienteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnderecoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CidadeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rpview
            // 
            this.rpview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpview.Location = new System.Drawing.Point(0, 0);
            this.rpview.Name = "rpview";
            this.rpview.Size = new System.Drawing.Size(872, 456);
            this.rpview.TabIndex = 0;
            this.rpview.ZoomPercent = 150;
            // 
            // ClienteBindingSource
            // 
            this.ClienteBindingSource.DataSource = typeof(ADVCore.Models.Cliente);
            // 
            // EnderecoBindingSource
            // 
            this.EnderecoBindingSource.DataSource = typeof(ADVCore.Models.Endereco);
            // 
            // CidadeBindingSource
            // 
            this.CidadeBindingSource.DataSource = typeof(ADVCore.Models.Cidade);
            // 
            // FormReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 456);
            this.Controls.Add(this.rpview);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormReport";
            this.Text = "Report";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.MaximumSizeChanged += new System.EventHandler(this.FormReport_MaximumSizeChanged);
            this.Load += new System.EventHandler(this.FormReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ClienteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnderecoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CidadeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource ClienteBindingSource;
        private System.Windows.Forms.BindingSource EnderecoBindingSource;
        private System.Windows.Forms.BindingSource CidadeBindingSource;
        private Microsoft.Reporting.WinForms.ReportViewer rpview;
    }
}