﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ADVCore.Models;
using ADVCore.Models.Concret;
using ADVCore.Service;
using Syncfusion.Linq;

namespace AdvWin
{
    public partial class FormUsuario : Form
    {
        private UsuarioService _us;
        private Usuario _user;
        private CidadeService _cs;
        public FormUsuario()
        {
            Inicializa();
         
        }

        private void Inicializa(Usuario usuario = null)
        {
            InitializeComponent();
            CriaCombo();
            _cs = new CidadeService();
            MontaCobEscritorios();
            _us = new UsuarioService(new UsuarioRepository());
            _user = usuario != null? usuario: new Usuario();
            if (_user.Id > 0)
            {
                PreencheCampos();
                paneldados.Visible = true;
            }
            if (_user.Tipo == TipoUser.ADMINISTRADOR)
            {
                toolStrip1.Enabled = true;
            }else
            {
                toolStrip1.Enabled = false;
            }
            
        }
        private void MontaCobEscritorios()
        {
            List<Escritorio> dados = _cs.Escritorios();
            cobEscritorio.Items.Clear();
            dados.ForEach(c => { cobEscritorio.Items.Add(c); });

        }
        public FormUsuario(Usuario usuario)
        {
            Inicializa(usuario);    
        }

        private void CriaCombo()
        {
            cobTipo.Items.Clear();
            cobTipo.Items.Add(TipoUser.ADMINISTRADOR);
            cobTipo.Items.Add(TipoUser.FUNCIONARIO);
            
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cobTipo.SelectedItem == null)
                {
                    MessageBox.Show("Tipo de usuário inválido.", this.Text, MessageBoxButtons.OK,
                        MessageBoxIcon.Asterisk);
                }

                if (cobEscritorio.SelectedItem == null)
                {
                    MessageBox.Show("A seleção do escritório é obrigatória", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cobEscritorio.Focus();
                    return;
                }
                _user.Login = txtLogin.Text;
                _user.Nome = txtNome.Text;
                _user.Senha = txtSenha.Text;
                _user.Tipo = (TipoUser) cobTipo.SelectedItem;
                _user.IdEscritorio = ((Escritorio)cobEscritorio.SelectedItem).Id;
                _us.Save(_user);

                _user = new Usuario();
                LimpaCampos();
                MessageBox.Show("Dados salvos com sucesso!", this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                paneldados.Visible = false;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
            }
        }

        private void LimpaCampos()
        {
            txtLogin.Text = "";
            txtNome.Text = "";
            txtSenha.Text = "";
            cobTipo.SelectedItem = null;
        }

        private void PreencheCampos()
        {
            txtLogin.Text = _user.Login;
            txtNome.Text = _user.Nome;
            txtSenha.Text = _user.Senha;
            cobTipo.SelectedItem = _user.Tipo;
            if (_user.IdEscritorio > 0)
            {
                var escritorios = cobEscritorio.Items.ToList<Escritorio>();
                Escritorio e = escritorios.Single(s => s.Id == _user.IdEscritorio);
                cobEscritorio.SelectedItem = e;
            }
        }

        private void btNovo_Click(object sender, EventArgs e)
        {
            _user = new Usuario();
            LimpaCampos();
            paneldados.Visible = true;
            txtNome.Focus();

        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            var resposta = MessageBox.Show("Deseja realmente cancelar a gravação dos dados?", this.Text,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (resposta == DialogResult.No) return;
            _user = new Usuario();
            LimpaCampos();
            paneldados.Visible = false;

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (cobResulte.SelectedItem == null)
            {
                MessageBox.Show("Deve ser selecionando um usuário no resultado da busca.", this.Text,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                cobResulte.Focus();
                return;
            }

            _user = (Usuario) cobResulte.SelectedItem;
            paneldados.Visible = true;
            PreencheCampos();

        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            if (txtTermo.Text.Trim().Equals(""))
            {
                MessageBox.Show("Não foi informado dados para busca.", this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                txtTermo.Focus();
                return;
            }

            cobResulte.Items.Clear();
            var dados = _us.FindByNome(txtTermo.Text).ToArray();
            cobResulte.Items.AddRange(dados);
            cobResulte.SelectedItem = null;
            cobResulte.Focus();
            cobResulte.Visible = true;
            toolStripButton1.Visible = true;


        }

        private void FormUsuario_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
