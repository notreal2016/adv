﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ADVCore.Context;
using ADVCore.Models;
using ADVCore.Service;

namespace AdvWin.Forms
{
    public partial class FormFicha : Form
    {
        private Cliente _cliente;
        private FrmCliente _formCliente;
        private ClienteService _service;
        private Historico _historicoEditado;
        private Telefone _telefoneEditado;
        private AdvContext _context;

        public FormFicha(FrmCliente frmCliente)
        {
            Inicializa(frmCliente, new Cliente());
            
        }

        public FormFicha(FrmCliente frmCliente, Cliente cliente)
        {
            Inicializa(frmCliente, cliente);
            PreencherFicha();
            
        }

        private void PreencherFicha()
        {
            txtNome.Text = _cliente.Nome;
            MaskCPF.Text = _cliente.Cpf;
            txtEndereco.Text = _cliente.End.Rua;
            txtNumero.Text = _cliente.End.Numero.ToString();
            CobCidade.SelectedItem = _cliente.End.Cidade;
            txtReferencia.Text = _cliente.Ponto;
            DtAnimersario.Value = _cliente.Aniversario;
            txtProcesso.Text = _cliente.Processo;
            txtEmail.Text = _cliente.Email;
            RtRelatorio.Text = _cliente.Relato;
            cobTipo.SelectedItem = _cliente.Tipo;
            txtBairro.Text = _cliente.End.Bairro;
            LbTelefones.Items.AddRange(_cliente.Telefones.ToArray());
            
            if (_cliente.Foto != null)
            {
                picImagem.Image = Util.ArrayToImage(_cliente.Foto);
            }
            
        }

        private void Inicializa(FrmCliente frmCliente, Cliente cliente)
        {
            InitializeComponent();
            
            _cliente = cliente;
            _formCliente = frmCliente;
            _service = new ClienteService();
            CobCidade.DataSource = _service.CidadesAll();
            CobCidade.SelectedItem = null;
            cobOperadora.Items.AddRange(_service.OperadprasAll().ToArray());
            cobTipo.Items.Clear();
            cobTipo.Items.Add(TipoCliente.CLIENTE);
            cobTipo.Items.Add(TipoCliente.CONTATO);
            cobTipo.Items.Add(TipoCliente.DIVERSOS);
        }
        
        

        private void button2_Click(object sender, EventArgs e)
        {
            var resposta = MessageBox.Show("Deseja sair do cadastro de cliente?", "Sair", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (resposta == DialogResult.Yes)
            {
                _formCliente.WindowState = FormWindowState.Maximized;
                _formCliente.Focus();
                Dispose(true);
            }
        }

        
        private void btCaptura_Click(object sender, EventArgs e)
        {
            Form chilForm = new frmCamera(picImagem);
            chilForm.Show();
            chilForm.WindowState = FormWindowState.Normal;
            
        }

        /*
        private void btMHistorico_Click(object sender, EventArgs e)
        {
            if (ValidaDados.ExistCaracterEspeciais(txtHistorico.Text)|| txtHistorico.Text.Trim().Equals(""))
            {
                MessageBox.Show("Informação do histórico contém dados inválidos.", "Inclusão de histórico",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if (_historicoEditado == null)
            {
                _historicoEditado = new Historico();}
            else
            {
                LbHistorico.Items.Remove(_historicoEditado);
            }
            _historicoEditado.Dados = txtHistorico.Text;
            _historicoEditado.Data = dtHistorico.Value;
            LbHistorico.Items.Add(_historicoEditado);
            _historicoEditado = null;
            LbHistorico.Refresh();
            txtHistorico.Text = "";
        }

        private void btEHistorico_Click(object sender, EventArgs e)
        {
            if (LbHistorico.SelectedItem == null)
            {
                MessageBox.Show("Não há históricos selecionados.", "Editar Histórico.", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return;
            }

            _historicoEditado = (Historico) LbHistorico.SelectedItem;
            txtHistorico.Text = _historicoEditado.Dados;
            dtHistorico.Value = _historicoEditado.Data;            
            LbHistorico.Refresh();
        }
        */
        private void btMTelefone_Click(object sender, EventArgs e)
        {
            if (txtDDD.Text.Trim().Equals("") || !ValidaDados.SoNumeros(txtDDD.Text.Replace("(", "").Replace(")", "")))
            {
                MessageBox.Show("DDD só pode contér números.", "Adicionando telefone", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtDDD.Focus();
                return;
            }

            if (txtTNumero.Text.Trim().Equals("") || !ValidaDados.SoNumeros(txtTNumero.Text.Replace(".", "").Replace("-","")))
            {
                MessageBox.Show("Número de telefone só pode contér números.", "Adicionando telefone", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtTNumero.Focus();
                return;
            }

            if (cobOperadora.SelectedItem == null)
            {
                MessageBox.Show("Selecione uma operadora.", "Adicionando telefone", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                cobOperadora.Focus();
                return;
            }
            if (_telefoneEditado == null) { _telefoneEditado = new Telefone(); }
            else
            {
                LbTelefones.Items.Remove(_telefoneEditado);
            }

            _telefoneEditado.Ddd = txtDDD.Text;
            _telefoneEditado.Numero = txtTNumero.Text;
            _telefoneEditado.Operadora = (Operadora) cobOperadora.SelectedItem;
            _telefoneEditado.Whatsapp = chWhatsapp.Checked;
            LbTelefones.Items.Add(_telefoneEditado);
            //LbTelefones.RefreshAll();
            txtDDD.Text = "";
            txtTNumero.Text = "";
            cobOperadora.SelectedItem = null;
            chWhatsapp.Checked = false;
            _telefoneEditado = null;

        }

        private void btETelefone_Click(object sender, EventArgs e)
        {
            if (LbTelefones.SelectedItem == null)
            {
                MessageBox.Show("Precisa selecionar um telefone para poder editar.", "Editando telefone",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            _telefoneEditado = (Telefone) LbTelefones.SelectedItem;
            txtDDD.Text = _telefoneEditado.Ddd;
            txtTNumero.Text = _telefoneEditado.Numero;
            cobOperadora.SelectedItem = _telefoneEditado.Operadora;
            chWhatsapp.Checked = _telefoneEditado.Whatsapp;
            LbTelefones.Refresh();
        }
        /*
        private void btHMenos_Click(object sender, EventArgs e)
        {
            if (LbHistorico.SelectedItem == null)
            {
                MessageBox.Show("Não há histórico selecionado para exclusão.", "Excluindo histórico.",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            var resposta = MessageBox.Show("Deseja realmente exluir este histórico?", "Exluir histórico.",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (resposta == DialogResult.OK)
            {
                LbHistorico.Items.Remove(LbHistorico.SelectedItem);
                LbHistorico.Refresh();

            }
        }
        */
        private void btTMenos_Click(object sender, EventArgs e)
        {
            if (LbTelefones.SelectedItem == null)
            {
                MessageBox.Show("Não há telefone selecionado para excluir.", "Excluir telefone.", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return;
            }

            var respota = MessageBox.Show("Deseja realmente excluir este telefone?", "Excluir telefone.",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (respota == DialogResult.OK)
            {
                LbTelefones.Items.Remove(LbTelefones.SelectedItem);
                LbTelefones.Refresh();
            }
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            if (CobCidade.SelectedItem == null || ((Cidade) CobCidade.SelectedItem).IdEscritorio == null)
            {
                MessageBox.Show(
                    "Cidade não selecionada ou não está definido um escritório para a cidade em questão. Favor ajustar os dados",
                    this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                CobCidade.Focus();
                return;
            }

            if (!ValidaDados.SoNumeros(txtNumero.Text) || Int32.Parse(txtNumero.Text) <=0 )
            {
                MessageBox.Show("Campo número do endereço deve ser informado, positivo e maior que zero.", "Gravando.",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                txtNumero.Focus();
                return;
            }

            if (_cliente.Id == 0 )
            {
                _cliente.End = new Endereco();
            }
            
            
            
            _cliente.End.Rua = txtEndereco.Text;
            _cliente.End.Numero = int.Parse(txtNumero.Text);
            _cliente.End.Cidade = (Cidade) CobCidade.SelectedItem;
            _cliente.End.Bairro = txtBairro.Text;
            _cliente.Nome = txtNome.Text;
            _cliente.Cpf = MaskCPF.Text ;
            _cliente.Ponto = txtReferencia.Text;
            _cliente.Aniversario = DtAnimersario.Value;
            _cliente.Processo = txtProcesso.Text;
            _cliente.Email = txtEmail.Text;
            _cliente.Relato = RtRelatorio.Text;
            _cliente.Tipo = (cobTipo.SelectedItem== null)?TipoCliente.CLIENTE: (TipoCliente)cobTipo.SelectedItem;
            
            _cliente.Telefones = LbTelefones.Items.OfType<Telefone>().ToList();
            if (picImagem.Image != null)
            {
                _cliente.Foto = Util.ImgToArray(picImagem.Image);
            }
            
            try
            {
                Valida.ValidaCliente(_cliente);
                _service.Save(_cliente);
                AtualizaFormCliente();
                Dispose(true);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Erro ao gravar.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }

        }

        
        private void FormFicha_FormClosing(object sender, FormClosingEventArgs e)
        {
            button2_Click(sender, e);
        }

        private void btCidade_Click(object sender, EventArgs e)
        {
            Form frmCidade = new FrmCidade(CobCidade);
            frmCidade.ShowDialog();
            frmCidade.Dispose();
            
        }

        
        private void btOperadora_Click(object sender, EventArgs e)
        {
            Form frmOperadora = new FrmOperadora(_context,cobOperadora);
            frmOperadora.Show();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (_cliente == null || _cliente.Id == 0)
            {
                MessageBox.Show(
                    "Você deve primeiro gravar os dados da ficha do cliente para depois poder imprimir a ficha.",
                    "Ficha impressa", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Form report = new FormReport("AdvWin.Report.FichaCliente.rdlc",
                true, _service.FichaCliente(_cliente));
            report.MdiParent = this.MdiParent;
            report.Show();
            report.WindowState = FormWindowState.Normal;
            report.WindowState = FormWindowState.Maximized;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (((MDIPrincipal) this.MdiParent).Usuario.Tipo != TipoUser.ADMINISTRADOR)
            {
                MessageBox.Show("Usuário sem autorização para exlcuir dados.", this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }
            if (_cliente.Id == 0)
            {
                MessageBox.Show("A atual ficha ainda não foi gravada.", "Exluir ficha", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
                
            }
            var resposta = MessageBox.Show("Deseja excluir a atual ficha do cliente?", "Excluir ficha",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (resposta == DialogResult.Yes)
            {
                try
                {
                    _service.Excluir(_cliente);
                    MessageBox.Show("A ficha foi excluida", "Excluir ficha", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    AtualizaFormCliente();
                    Dispose();

                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Excluir ficha", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                }
                
            }
        }

        private void AtualizaFormCliente()
        {
            _formCliente.CriaListaClientes(_formCliente.Usuario.Tipo != TipoUser.ADMINISTRADOR?_formCliente.Usuario.IdEscritorio:0);
            _formCliente.WindowState = FormWindowState.Maximized;
            _formCliente.Focus();

        }

        private void FormFicha_Load(object sender, EventArgs e)
        {

        }
    }
}
