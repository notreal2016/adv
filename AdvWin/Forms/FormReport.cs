﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace AdvWin.Forms
{
    public partial class FormReport : Form
    {
        
        public FormReport(string path, bool isEmbeddedResource, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters = null)
        {
            
            InitializeComponent();
            // path + isEmbeddedResource.
            if (isEmbeddedResource)
                this.rpview.LocalReport.ReportEmbeddedResource = path;
            else
                this.rpview.LocalReport.ReportPath = path;

            // dataSources.
            foreach (var dataSource in dataSources)
            {
                var reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource(dataSource.Key, dataSource.Value);
                this.rpview.LocalReport.DataSources.Add(reportDataSource);
            }

            // reportParameters.
            if (reportParameters != null)
            {
                var reportParameterCollection = new List<Microsoft.Reporting.WinForms.ReportParameter>();

                foreach (var parameter in reportParameters)
                {
                    var reportParameter = new Microsoft.Reporting.WinForms.ReportParameter(parameter.Key, parameter.Value.ToString());
                    reportParameterCollection.Add(reportParameter);
                }

                this.rpview.LocalReport.SetParameters(reportParameterCollection);
            }

            rpview.SetDisplayMode(DisplayMode.PrintLayout);
        }

        private void FormReport_Load(object sender, EventArgs e)
        {

            this.rpview.RefreshReport();
            this.rpview.RefreshReport();
        }

        public static void ShowReport(string path, bool isEmbeddedResource, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters = null)
        {
            var formRelatorio = new FormReport(path, isEmbeddedResource, dataSources, reportParameters);
            formRelatorio.Show();
        }

        private void FormReport_MaximumSizeChanged(object sender, EventArgs e)
        {
            rpview.Height = this.Height;
            rpview.Width = this.Width;
        }
    }
}
