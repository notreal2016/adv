﻿using System;
using System.Collections.Generic;
using System.Linq;
using ADVCore.Models;
using ADVCore.Models.Concret;
using ADVCore.Service;

namespace AdvWin.Forms
{
    internal class Valida
    {
        public static void ValidaCliente(Cliente cliente)
        {
            ValidaString(cliente.Nome, "Nome");
            ValidaString(cliente.End.Rua, "Endereço");
            ValidaString(cliente.End.Bairro, "Bairro do endereço");
            ValidaCPF(cliente.Cpf);
            ValidaTelefones(cliente.Telefones.ToList());
        }

        private static void ValidaString(String nome, String campo)
        {
            if (ValidaDados.ExistCaracterEspeciais(nome))
            {
                throw new ClienteException(campo + " do cliente não pode conter caracteres especiais.");
            }

            if (nome.Trim().Equals(""))
            {
                throw  new ClienteException(campo + " do cliente não pode ficar em branco.");
            }

            if (nome == null)
            {
                throw new ClienteException(campo + " do cliente não pode ser um valor nulo.");
            }
        }

        private static void ValidaCPF(String cpf)
        {
            if (!ValidaDados.SoNumeros(cpf))
            {
                throw  new ClienteException("O Campo CPF só pode conter números.");
            }

            if (!ValidaDados.ValidaCpf(cpf))
            {
                throw new ClienteException("CPF informado é inválido.");
            }
        }

        private static void ValidaTelefones(List<Telefone> telefones)
        {
            if (telefones.Count == 0)
            {
                throw  new ClienteException("O cliente deve conter ao menos um número de telefone.");
            }
        }

        private static void ValidaFoto(Object foto)
        {
            if (foto == null)
            {
                throw  new ClienteException("O cliente deve ter uma foto anexada a sua ficha.");
            }
        }

        
    }
}