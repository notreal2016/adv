﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls.Adapters;
using System.Windows.Forms;
using AForge.Video.DirectShow;

namespace AdvWin.Forms
{
    public partial class frmCamera : Form
    {

        // Considerar a primeira câmera que for encontrada no sistema
        private const int VIDEODEVICE = 0;
        // Resolucao de 640x480, 24 bits por pixel. A sua câmera tem que suportar essa resolução, senão, altere para uma resolução suportada.
        private const int VIDEOWIDTH = 640;
        private const int VIDEOHEIGHT = 480;
        private const int VIDEOBITSPERPIXEL = 24;
        private PictureBox _origem;
        
        private VideoCaptureDevice videoSource;
        public frmCamera(PictureBox origem)
        {
            InitializeComponent();
            _origem = origem;
            FilterInfoCollection videosources = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            MontaCombo(videosources);
            MontaCamera(CobCameras.SelectedIndex);
        }

        private void MontaCombo(FilterInfoCollection cameras)
        {
            CobCameras.DataSource = null;
            List<String> cam = new List<string>();
            foreach (FilterInfo camera in cameras)
            {
                cam.Add(camera.Name);              
            }

            CobCameras.DataSource = cam;
        }
        private void MontaCamera(int idCamera)
        {
            FilterInfoCollection videosources = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            if (videosources != null)
            {
                try
                {
                    videoSource = new VideoCaptureDevice(videosources[idCamera].MonikerString);
                    videoSource.NewFrame += (s, e) =>
                    {
                        if (CameraBox.Image != null)
                        {
                            CameraBox.Image.Dispose();
                        }

                        CameraBox.Image = (Bitmap) e.Frame.Clone();
                    };
                    videoSource.Start();
                }
                catch (Exception e)
                {
                    if (e.HResult == -2146233086)
                    {
                        MessageBox.Show("Não consegui localizar a sua webcam. " +
                                        "Favor ferifique se a mesma está instalada e en bom funcionamento.",
                            "Erro ao localizar webCam.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(e.Message, "Erro ao localizar webCam.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                finally
                {
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _origem.Image = CameraBox.Image;
            if (videoSource != null && videoSource.IsRunning)
            {
                videoSource.SignalToStop();
                videoSource = null;
            }
            Dispose(true);
        }

        private void frmCamera_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (videoSource != null && videoSource.IsRunning)
            {
                videoSource.SignalToStop();
                videoSource = null;
            }
            base.OnClosed(e);
        }
        

        private void button2_Click(object sender, EventArgs e)
        {
            MontaCamera(CobCameras.SelectedIndex);
        }
    }
}
