﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ADVCore.Context;
using ADVCore.Models;
using ADVCore.Service;

namespace AdvWin.Forms
{
    public partial class FrmOperadora : Form
    {
        private OperadoraService service ;
        private Operadora _selecionada, _editadaOperadora;
        private ComboBox _combo;
        public FrmOperadora(AdvContext context)
        {
            Inicializa(context);
        }

        public FrmOperadora(AdvContext context,ComboBox combo)
        {
            _combo = combo;
            _selecionada = (Operadora) combo.SelectedItem;
            Inicializa(context);
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (txtOperadora.Text.Trim().Equals(""))
            {
                MessageBox.Show("Deve ser digitado o nome da nova operadora", "Incluir operadora", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtOperadora.Focus();
                return;
            }

            try
            {
                ValidaDados.ExistCaracterEspeciais(txtOperadora.Text);
                if (_editadaOperadora == null)
                {
                    _editadaOperadora = new Operadora(txtOperadora.Text.ToUpper());
                }
                else
                {
                    _editadaOperadora.Poderadora = txtOperadora.Text;
                }
                
                service.Save(_editadaOperadora);
                LbOperadoras.DataSource = service.Operadoras().ToList();
                txtOperadora.Text = "";
                _editadaOperadora = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Inserindo operadora", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            if (LbOperadoras.SelectedItem == null)
            {
                MessageBox.Show("Nenhuma operadora foi selecionada", "Editando operadora", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            _editadaOperadora = (Operadora) LbOperadoras.SelectedItem;
            txtOperadora.Text = _editadaOperadora.Poderadora;
            txtOperadora.Focus();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (_combo != null)
            {
                _combo.DataSource = null;
                _combo.DataSource = service.Operadoras().ToList();
                _combo.SelectedItem = _selecionada;
                _combo.Refresh();
            }
            Dispose();
        }

        private void Inicializa(AdvContext context)
        {
            InitializeComponent();
            service = new OperadoraService(context);
            LbOperadoras.DataSource = service.Operadoras().ToList();
        }
    }
}
