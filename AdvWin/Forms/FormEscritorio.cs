﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ADVCore.Models;
using ADVCore.Models.Concret;
using ADVCore.Service;
using SchequesWin.Forms;
using Syncfusion.Linq;


namespace AdvWin.Forms
{
    public partial class FormEscritorio : FormModelo
    {
        private Escritorio _escritorio;
        private CidadeService _cs;
        private EscritorioService _es;
        public FormEscritorio()
        {
            InitializeComponent();
            _cs = new CidadeService();
            _es = new EscritorioService();
            MontaCobCidades();
            MontaCobEscritorios();
        }

        private void btNovo_Click(object sender, EventArgs e)
        {
            _escritorio = new Escritorio();
            PreencheCampos();
            AtivaEdicao(true);

        }

        private void AtivaEdicao(bool ativa)
        {
            barraFerramentas.Enabled = !ativa;
            panelContainer.Visible = ativa;
            panelContainer.Enabled = ativa;
            txtNome.Focus();
        }
        private void PreencheCampos()
        {
            txtNome.Text = _escritorio.Nome;
            txtBairro.Text = _escritorio.Endereco.Bairro;
            txtRua.Text = _escritorio.Endereco.Rua;
            txtNumero.Text = _escritorio.Endereco.Numero.ToString();
            txtTelefone.Text = _escritorio.Telefone;
            cobCidade.SelectedItem = _escritorio.Endereco.Cidade;
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            var resposta = MessageBox.Show("Deseja realmente cancelar a edição dos dados?", this.LabTitulo.Text,
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (resposta == DialogResult.Yes)
            {
                barraFerramentas.Enabled = true;
                AtivaEdicao(false);
            }
        }

        private void MontaCobCidades()
        {
            var dados = _cs.Cidades();
            cobCidade.Items.Clear();
            dados.ForEach(c => { cobCidade.Items.Add(c); });
            cobCidade.SelectedItem = null;
        }

        private void MontaCobEscritorios()
        {
            var dados =  _cs.Escritorios();
            cobEscritorios.Items.Clear();
            dados.ForEach(e => { cobEscritorios.Items.Add(e); });
            cobEscritorios.SelectedItem = null;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (cobEscritorios.ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Selecione primeiro um escritório para poder editar seus dadaos.", this.labTitulo.Text,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                cobEscritorios.Focus();
                return;
            }

            _escritorio = (Escritorio) cobEscritorios.ComboBox.SelectedItem;
            PreencheCampos();
            AtivaEdicao(true);
        }

        private void btSalvar_Click(object sender, EventArgs e)
        {
            
            if (txtNome.Text == null || txtNome.Text.Trim().Equals(""))
            {
                MessageBox.Show("Compo nome deve ser preenchido.", labTitulo.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNome.Focus();
                return;
            }

            _escritorio.Nome = txtNome.Text;
            _escritorio.Endereco.Bairro = txtBairro.Text;
            _escritorio.Endereco.Cidade = (Cidade) cobCidade.SelectedItem;
            _escritorio.Endereco.Numero = int.Parse(txtNumero.Text);
            _escritorio.Endereco.Rua = txtRua.Text;
            _escritorio.Telefone = txtTelefone.Text;
            _escritorio =  _es.Salva(_escritorio);
            MontaCobEscritorios();
            AtivaEdicao(false);
            MessageBox.Show("Dados salvo com sucesso!", labTitulo.Text, MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            cobEscritorios.Focus();
        }
    }
}
