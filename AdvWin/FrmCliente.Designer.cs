﻿namespace AdvWin
{
    partial class FrmCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCliente));
            this.btNovo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.LTermo = new System.Windows.Forms.ToolStripLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.txtTermo = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.CobTipo = new System.Windows.Forms.ToolStripComboBox();
            this.btBuscar = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.aniversariantesDoMêsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fichaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cooperadorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btRefresh = new System.Windows.Forms.ToolStripButton();
            this.LabRegistros = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cobEscritorio = new System.Windows.Forms.ToolStripComboBox();
            this.btEscritorio = new System.Windows.Forms.ToolStripButton();
            this.directorySearcher1 = new System.DirectoryServices.DirectorySearcher();
            this.LBDados = new System.Windows.Forms.ListBox();
            this.btTodos = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btNovo
            // 
            this.btNovo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btNovo.Image = ((System.Drawing.Image)(resources.GetObject("btNovo.Image")));
            this.btNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(28, 28);
            this.btNovo.Text = "Nova ficha";
            this.btNovo.Click += new System.EventHandler(this.BtNovo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // LTermo
            // 
            this.LTermo.Name = "LTermo";
            this.LTermo.Size = new System.Drawing.Size(94, 28);
            this.LTermo.Text = "Termo de busca:";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btNovo,
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.LTermo,
            this.txtTermo,
            this.toolStripLabel2,
            this.CobTipo,
            this.btBuscar,
            this.toolStripButton2,
            this.toolStripSeparator2,
            this.toolStripSplitButton1,
            this.btRefresh,
            this.LabRegistros,
            this.toolStripLabel1,
            this.cobEscritorio,
            this.btEscritorio,
            this.btTodos});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1261, 31);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton1.Text = "Editar ficha";
            this.toolStripButton1.Click += new System.EventHandler(this.BtEditar_Click);
            // 
            // txtTermo
            // 
            this.txtTermo.Name = "txtTermo";
            this.txtTermo.Size = new System.Drawing.Size(200, 31);
            this.txtTermo.ToolTipText = "Digite o que deseja buscar";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(84, 28);
            this.toolStripLabel2.Text = "Tipo de busca:";
            // 
            // CobTipo
            // 
            this.CobTipo.Name = "CobTipo";
            this.CobTipo.Size = new System.Drawing.Size(150, 31);
            // 
            // btBuscar
            // 
            this.btBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btBuscar.Image")));
            this.btBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btBuscar.Name = "btBuscar";
            this.btBuscar.Size = new System.Drawing.Size(28, 28);
            this.btBuscar.Text = "Busca";
            this.btBuscar.Click += new System.EventHandler(this.btBuscar_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton2.Text = "Desfaz";
            this.toolStripButton2.Click += new System.EventHandler(this.BtDesfazer_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aniversariantesDoMêsToolStripMenuItem,
            this.fichaToolStripMenuItem,
            this.filtroToolStripMenuItem,
            this.cooperadorToolStripMenuItem});
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(40, 28);
            this.toolStripSplitButton1.Text = "toolStripSplitButton1";
            // 
            // aniversariantesDoMêsToolStripMenuItem
            // 
            this.aniversariantesDoMêsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aniversariantesDoMêsToolStripMenuItem.Image")));
            this.aniversariantesDoMêsToolStripMenuItem.Name = "aniversariantesDoMêsToolStripMenuItem";
            this.aniversariantesDoMêsToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.aniversariantesDoMêsToolStripMenuItem.Text = "Aniversariantes do mês";
            this.aniversariantesDoMêsToolStripMenuItem.ToolTipText = "Aniversariantes";
            this.aniversariantesDoMêsToolStripMenuItem.Click += new System.EventHandler(this.aniversariantesDoMêsToolStripMenuItem_Click);
            // 
            // fichaToolStripMenuItem
            // 
            this.fichaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("fichaToolStripMenuItem.Image")));
            this.fichaToolStripMenuItem.Name = "fichaToolStripMenuItem";
            this.fichaToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.fichaToolStripMenuItem.Text = "Ficha";
            this.fichaToolStripMenuItem.ToolTipText = "Ficha do cliente selecionado";
            this.fichaToolStripMenuItem.Click += new System.EventHandler(this.fichaToolStripMenuItem_Click);
            // 
            // filtroToolStripMenuItem
            // 
            this.filtroToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("filtroToolStripMenuItem.Image")));
            this.filtroToolStripMenuItem.Name = "filtroToolStripMenuItem";
            this.filtroToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.filtroToolStripMenuItem.Text = "Filtro";
            this.filtroToolStripMenuItem.ToolTipText = "Lista o filtro de clientes";
            this.filtroToolStripMenuItem.Click += new System.EventHandler(this.filtroToolStripMenuItem_Click);
            // 
            // cooperadorToolStripMenuItem
            // 
            this.cooperadorToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cooperadorToolStripMenuItem.Image")));
            this.cooperadorToolStripMenuItem.Name = "cooperadorToolStripMenuItem";
            this.cooperadorToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.cooperadorToolStripMenuItem.Text = "Colaboradores";
            this.cooperadorToolStripMenuItem.Click += new System.EventHandler(this.cooperadorToolStripMenuItem_Click);
            // 
            // btRefresh
            // 
            this.btRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btRefresh.Image")));
            this.btRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.Size = new System.Drawing.Size(28, 28);
            this.btRefresh.Text = "Sincroniza dados ";
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // LabRegistros
            // 
            this.LabRegistros.Name = "LabRegistros";
            this.LabRegistros.Size = new System.Drawing.Size(55, 28);
            this.LabRegistros.Text = "Registros";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(59, 28);
            this.toolStripLabel1.Text = "Escritório:";
            this.toolStripLabel1.Click += new System.EventHandler(this.toolStripLabel1_Click);
            // 
            // cobEscritorio
            // 
            this.cobEscritorio.Name = "cobEscritorio";
            this.cobEscritorio.Size = new System.Drawing.Size(121, 31);
            // 
            // btEscritorio
            // 
            this.btEscritorio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btEscritorio.Image = ((System.Drawing.Image)(resources.GetObject("btEscritorio.Image")));
            this.btEscritorio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEscritorio.Name = "btEscritorio";
            this.btEscritorio.Size = new System.Drawing.Size(28, 28);
            this.btEscritorio.Text = "Aplica filtro Escritorio";
            this.btEscritorio.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // directorySearcher1
            // 
            this.directorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // LBDados
            // 
            this.LBDados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBDados.FormattingEnabled = true;
            this.LBDados.Location = new System.Drawing.Point(0, 31);
            this.LBDados.Name = "LBDados";
            this.LBDados.Size = new System.Drawing.Size(1261, 376);
            this.LBDados.TabIndex = 1;
            this.LBDados.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LBDados_MouseDoubleClick);
            // 
            // btTodos
            // 
            this.btTodos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btTodos.Image = ((System.Drawing.Image)(resources.GetObject("btTodos.Image")));
            this.btTodos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btTodos.Name = "btTodos";
            this.btTodos.Size = new System.Drawing.Size(28, 28);
            this.btTodos.Text = "Todos os escritorios";
            this.btTodos.Click += new System.EventHandler(this.btTodos_Click);
            // 
            // FrmCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 407);
            this.Controls.Add(this.LBDados);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCliente";
            this.Text = "Clientes";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCliente_FormClosing);
            this.Load += new System.EventHandler(this.FrmCliente_Load);
            this.SizeChanged += new System.EventHandler(this.FrmCliente_SizeChanged);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton btNovo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel LTermo;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripTextBox txtTermo;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox CobTipo;
        private System.Windows.Forms.ToolStripButton btBuscar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.DirectoryServices.DirectorySearcher directorySearcher1;
        private System.Windows.Forms.ListBox LBDados;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem fichaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filtroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aniversariantesDoMêsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btRefresh;
        //private static System.Windows.Forms.ToolStripProgressBar tBarra;
        private System.Windows.Forms.ToolStripLabel LabRegistros;
        private System.Windows.Forms.ToolStripMenuItem cooperadorToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cobEscritorio;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton btEscritorio;
        private System.Windows.Forms.ToolStripButton btTodos;
    }
}