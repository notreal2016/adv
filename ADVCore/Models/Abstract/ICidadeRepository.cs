﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADVCore.Models.Abstract
{
    public interface ICidadeRepository
    {
        void Save(Cidade cidade);
        void Remove(Int64 idCidade);
        IEnumerable<Cidade> FindAll();
    }
}