﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Service;

namespace ADVCore.Models.Abstract
{
    public interface IClienteRepository
    {
        void Save(Cliente cliente);
        IEnumerable<Cliente> FindAll();
        Cliente FindByID(Int64 id);
        Cliente FindByCPF(String cpf);
        IEnumerable<Cliente> FindByName(String nome);
        Cliente FindByProcesso(String processo);
        IEnumerable<Cliente> FindByCidade(String cidade);
        void Remove(Int64 idCliente);
        IEnumerable<InformeCliente> FindAniversariantes(Int64 mes);
    }
}