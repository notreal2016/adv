﻿using System.Collections.Generic;

namespace ADVCore.Models.Concret
{
    public interface IEscritorioRepository
    {
        /// <summary>
        /// Busca por um escritório aprtir do id repassado como paramentro
        /// </summary>
        /// <param name="id">Id do escritório</param>
        /// <returns>Escritório localizado ou null</returns>
        Escritorio FindById(long id);

        /// <summary>
        /// Busca por todos os escritórios cadastrados
        /// </summary>
        /// <returns>Lista em ordem alfabetica dos escritórios cadastrados</returns>
        List<Escritorio> FindAll();

        /// <summary>
        /// Persiste os dados de um escritório no banco de dados 
        /// </summary>
        /// <param name="escritorio">Escritório cujos dados serão persitidos</param>
        /// <returns>Escritório após os dados persistidos</returns>
        Escritorio Salva(Escritorio escritorio);

        /// <summary>
        /// Remove os dados de um escritório pelo id repassado
        /// </summary>
        /// <param name="id">Id do escritório cujos dados serão removidos</param>
        void Remove(long id);
    }
}