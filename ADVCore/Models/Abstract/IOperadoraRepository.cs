﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADVCore.Models.Abstract
{
    public interface IOperadoraRepository
    {
        void Save(Operadora operadora);
        IEnumerable<Operadora> FindAll();
        void Remove(Int64 idOperadora);
    }
}