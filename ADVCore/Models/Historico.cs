﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ADVCore.Models
{
    /// <summary>
    /// Classe encarregada de guardar os dados de um histórico de um cliente
    /// </summary>
    [Table("Historico")]
    public class Historico
    {
        /*[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _historicoId;*/
        [Required(ErrorMessage = "Por favor informe os dados referene ao histórico criado.")]
        private String _dados;

        private DateTime _data;

        private Int64 _clienteID;

        /// <summary>
        /// Histórico do cliente
        /// </summary>
        /// <param name="dados">Dados do historico</param>
        /// <param name="data">Data do histórico</param>
        public Historico(string dados, DateTime data)
        {
            _dados = dados;
            _data = data;
        }


        public Historico()
        {
        }


        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long HistoricoId { get; set; }

        public string Dados
        {
            get { return _dados; }
            set { _dados = value; }
        }

        public DateTime Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public long ClienteID
        {
            get { return _clienteID; }
            set { _clienteID = value; }
        }

        [ForeignKey("ClienteID")] public virtual Cliente Cliente { get; set; }

        public override string ToString()
        {
            return _data.ToShortDateString() + " - " + _dados.ToUpper();
        }
    }
}