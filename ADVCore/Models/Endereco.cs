﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;

namespace ADVCore.Models
{
    /// <summary>
    /// Classse encarregada de guardar dados de endereço
    /// </summary>
    [Table("Endereco"), Serializable, JsonObject]
    public class Endereco
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _enderecoId;

        [Required(ErrorMessage = "Por favor informe o nome da rua do endereço do cliente.")]
        private String _rua;

        [Required(ErrorMessage = "Informe o número do endereço do cliente.")]
        private int _numero;

        [Required(ErrorMessage = "Por favor informe o bairro do endereço do cliente.")]
        private String _bairro;

        //[Required]
        private String _CEP;

        /// <summary>
        /// Controe um objeto endereço com os dados repassador pelo construtor 
        /// </summary>
        /// <param name="rua">Endereço</param>
        /// <param name="numero">Número</param>
        /// <param name="bairro">Bairro</param>
        /// <param name="ciadade">Cidade do endereço</param>
        public Endereco(string rua, int numero, string bairro, Cidade cidade)
        {
            _rua = rua;
            _numero = numero;
            _bairro = bairro;
            Cidade = cidade;
        }


        public long EnderecoId
        {
            get { return _enderecoId; }
            set { _enderecoId = value; }
        }

        public Endereco()
        {
        }

        public virtual Cidade Cidade { get; set; }

        public string Cep
        {
            get { return _CEP; }
            set { _CEP = value; }
        }

        public string Rua
        {
            get { return _rua; }
            set { _rua = value; }
        }

        public int Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }

        public string Bairro
        {
            get { return _bairro; }
            set { _bairro = value; }
        }
    }
}