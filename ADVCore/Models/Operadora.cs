﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;


namespace ADVCore.Models
{
    /// <summary>
    /// Classe encarregada de armazenar dados de uma operadora;
    /// </summary>
    [Table("Operadora"), Serializable, JsonObject]
    public class Operadora
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private int _id;

        [Required(ErrorMessage = "Por favor informar o nome da operadora com no máximo 20 caracteres.")]
        private String _poderadora;

        public Operadora()
        {
        }

        public Operadora(string poderadora)
        {
            _poderadora = poderadora;
        }

        public int Id
        {
            get { return _id; }
            private set { _id = value; }
        }

        [Index("IX_Cpf", 2, IsUnique = true), MaxLength(20)]
        public string Poderadora
        {
            get { return _poderadora; }
            set { _poderadora = value; }
        }

        public override string ToString()
        {
            return this._poderadora.ToUpper();
        }
    }
}