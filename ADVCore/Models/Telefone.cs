﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ADVCore.Models
{
    /// <summary>
    /// Classe encarregada em armazenar dados de um telefone
    /// </summary>
    [Table("Telefone"), Serializable, JsonObject]
    public class Telefone
    {
        /*[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _telefoneId;*/
        [Required(ErrorMessage = "Por favor informar o DDD do telefone no seguinte formato:000"), MaxLength(3)]
        private string _ddd;

        [Required(ErrorMessage =
             "Por favor informe o número do telefone conforme com no máximo 10 digitos. Somente deve ser usado números"),
         MaxLength(10)]
        private string _numero;

        [Required] private bool _whatsapp;

        private Int64 _clienteID;


        /// <summary>
        /// Criação de um objeto telefone a partir dos dados repassado
        /// </summary>
        /// <param name="ddd">DDD</param>
        /// <param name="numero">Número do telefone</param>
        /// <param name="operadora">Operadora</param>
        /// <param name="whatsapp">Se o número é Whatapp</param>
        public Telefone(string ddd, string numero, Operadora operadora, bool whatsapp = false)
        {
            _ddd = ddd;
            _numero = numero;
            Operadora = operadora;
            _whatsapp = whatsapp;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 TelefoneId { get; set; }

        public Telefone()
        {
            _whatsapp = false;
        }


        public string Ddd
        {
            get { return _ddd; }
            set { _ddd = value; }
        }

        public string Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }


        public bool Whatsapp
        {
            get { return _whatsapp; }
            set { _whatsapp = value; }
        }

        public override string ToString()
        {
            return _ddd + " - " + _numero + " Operadora: "
                   + Operadora.Poderadora.ToUpper() + " Whatsapp: " + ((_whatsapp) ? "SIM" : "NÃO");
        }

        public long ClienteID
        {
            get { return _clienteID; }
            set { _clienteID = value; }
        }

        [ForeignKey("ClienteID")] public virtual Cliente Cliente { get; set; }
        public virtual Operadora Operadora { get; set; }

        [NotMapped]
        public String NomeOperadora
        {
            get { return Operadora.Poderadora; }
            private set { }
        }
    }
}