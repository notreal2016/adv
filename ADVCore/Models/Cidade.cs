﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ADVCore.Models
{
    /// <summary>
    /// classe encarregada de guardar os dados de nome.
    /// </summary>
    [Table("Cidade"), Serializable, JsonObject]
    public class Cidade
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private int _id;

        [Required(ErrorMessage = "Por favor informe o nome da cidade."), MaxLength(50)]
        private String _nome;

        [Required(ErrorMessage = "Por favor informe a UF do estado da cidade."), MaxLength(2)]
        private String _uf;

        private long? _idEscritorio;

        
       

        /// <summary>
        /// Controe uma classe nome
        /// </summary>
        /// <param name="nome">nome da nome</param>
        /// <param name="uf">Estado da nome</param>
        public Cidade(string nome, string uf)
        {
            _nome = nome;
            _uf = uf;
        }

        /// <summary>
        /// Cria uma classe em branco.
        /// </summary>
        public Cidade()
        {
        }

        public long? IdEscritorio
        {
            get => _idEscritorio;
            set => _idEscritorio = value;
        }
        
        
        
        public int Id
        {
            get { return _id; }
            private set { _id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public string Uf
        {
            get { return _uf; }
            set { _uf = value; }
        }

        public override string ToString()
        {
            return _nome.ToUpper();
        }
    }
}