﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Context;

namespace ADVCore.Models.Context
{
    public class FactoryContext
    {
        private static AdvContext _context;

        public static AdvContext GetContext()
        {
            if (_context == null)
            {
                _context = new AdvContext();
            }

            return _context;
        }
    }
}
