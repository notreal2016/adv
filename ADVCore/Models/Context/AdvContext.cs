﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Models;
using MySql.Data.MySqlClient;

namespace ADVCore.Context
{
    public class AdvContext : DbContext
    {
        public AdvContext()
        {
            var chamada = new MySqlProviderServices();
            this.ChangeTracker.DetectChanges();
            this.Configuration.AutoDetectChangesEnabled = true;
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Cidade> Cidades { get; set; }
        public DbSet<Historico> Historicos { get; set; }
        public DbSet<Operadora> Operadoras { get; set; }
        public DbSet<Telefone> Telefones { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Escritorio> Escritorios { get; set; }


        public override int SaveChanges()
        {
            //Processo reremoção dos campos orfãos.
            var orphanedResponses = ChangeTracker.Entries().Where(
                e => (e.State == EntityState.Modified || e.State == EntityState.Added) &&
                     e.Entity is Historico &&
                     e.Reference("Cliente").CurrentValue == null);
            foreach (var orphanedResponse in orphanedResponses)
            {
                Historicos.Remove(orphanedResponse.Entity as Historico);
            }

            var orphanedResponses2 = ChangeTracker.Entries().Where(
                e => (e.State == EntityState.Modified || e.State == EntityState.Added) &&
                     e.Entity is Telefone &&
                     e.Reference("Cliente").CurrentValue == null);
            foreach (var orphanedTelefone in orphanedResponses2)
            {
                Telefones.Remove(orphanedTelefone.Entity as Telefone);
            }

            return base.SaveChanges();
        }
    }
}