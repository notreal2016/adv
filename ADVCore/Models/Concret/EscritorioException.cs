﻿using System;
using System.Runtime.Serialization;

namespace ADVCore.Models.Concret
{
    [Serializable]
    internal class EscritorioException : Exception
    {
        public EscritorioException()
        {
        }

        public EscritorioException(string message) : base(message)
        {
        }

        public EscritorioException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EscritorioException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}