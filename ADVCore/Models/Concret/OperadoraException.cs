﻿using System;
using System.Runtime.Serialization;

namespace ADVCore.Models.Concret
{
    [Serializable]
    internal class OperadoraException : Exception
    {
        public OperadoraException()
        {
        }

        public OperadoraException(string message) : base(message)
        {
        }

        public OperadoraException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OperadoraException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}