﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Context;
using ADVCore.Models.Abstract;
using ADVCore.Models.Context;

namespace ADVCore.Models.Concret
{
    /// <summary>
    /// Classe encarregada de persistir os dados de uma operadora no banco de dados
    /// </summary>
    public class OperadoraRepository : IOperadoraRepository
    {
        private AdvContext _context = FactoryContext.GetContext();

        /// <summary>
        /// Cria um objeto OperadoraRepository
        /// </summary>
        /// <param name="context">Contexto de persistência</param>
        public OperadoraRepository()
        {

        }

        /// <summary>
        /// Salva os dados de uma operadora no banco de dados.
        /// </summary>
        /// <param name="operadora">Operadora cujos dados serão salvos</param>
        public void Save(Operadora operadora)
        {
            using (DbContextTransaction transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (operadora.Id == 0)
                    {
                        _context.Operadoras.Add(operadora);
                    }
                    else
                    {
                        _context.Entry(operadora).State = EntityState.Modified;
                    }

                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    if (e.HResult == -2146233087)
                    {
                        throw new OperadoraException("Erro, já exite operadora cadastrada no sistema como: " +
                                                     operadora.Poderadora + ".");
                    }

                    throw new ClienteException("Não foi possível gravar os dados da operadora," +
                                               " favor contatar suporte repassando o erro a seguir. Erro: "
                                               + e.HResult + " - " + e.Message);
                }
            }
        }

        /// <summary>
        /// Retorna uma lista de operadoras em ordem alfabetica
        /// </summary>
        /// <returns>Enumerable de operadoras</returns>
        public IEnumerable<Operadora> FindAll()
        {
            return _context.Operadoras.OrderBy(operadora => operadora.Poderadora);
        }

        /// <summary>
        /// Remove uma operadora do banco de dados
        /// </summary>
        /// <param name="idOperadora">Id da operadora a ser desacoplada</param>
        public void Remove(long idOperadora)
        {
            using (DbContextTransaction transaction = _context.Database.BeginTransaction())
            {
                Operadora operadora = _context.Operadoras.Find(idOperadora);
                try
                {
                    _context.Operadoras.Remove(operadora);
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw new OperadoraException(e.Message);
                }
            }
        }
    }
}