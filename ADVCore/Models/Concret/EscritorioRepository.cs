﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Context;

namespace ADVCore.Models.Concret
{
    public class EscritorioRepository : IEscritorioRepository
    {
        private AdvContext _context;
        /// <summary>
        /// Cria uma classe do tipo EscritórioRepository
        /// </summary>
        /// <param name="context">Contexto de criação</param>
        public EscritorioRepository(AdvContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Busca por um escritório aprtir do id repassado como paramentro
        /// </summary>
        /// <param name="id">Id do escritório</param>
        /// <returns>Escritório localizado ou null</returns>
        public Escritorio FindById(long id)
        {
            return _context.Escritorios.Find(id);
        }
        /// <summary>
        /// Busca por todos os escritórios cadastrados
        /// </summary>
        /// <returns>Lista em ordem alfabetica dos escritórios cadastrados</returns>
        public List<Escritorio> FindAll()
        {
            return _context.Escritorios.OrderBy(e => e.Nome).ToList();

        }
        /// <summary>
        /// Persiste os dados de um escritório no banco de dados 
        /// </summary>
        /// <param name="escritorio">Escritório cujos dados serão persitidos</param>
        /// <returns>Escritório após os dados persistidos</returns>
        public Escritorio Salva(Escritorio escritorio)
        {
            using (var transacao = _context.Database.BeginTransaction())
            {
                try
                {
                    
                    if (escritorio.Id == 0)
                    {
                        _context.Escritorios.Add(escritorio);
                    }
                    else
                    {
                        _context.Entry(escritorio).State = EntityState.Modified;
                    
                    }

                    _context.SaveChanges();
                    transacao.Commit();
                    return escritorio;
                }
                catch (Exception e)
                {
                    transacao.Rollback();
                    throw new EscritorioException("Erro ao persistir os dados de um escritório. Erro:" + e.HResult + " - " + e.Message );
                }
                
            }
        }
        /// <summary>
        /// Remove os dados de um escritório pelo id repassado
        /// </summary>
        /// <param name="id">Id do escritório cujos dados serão removidos</param>
        public void Remove(long id)
        {
            using (var transacao = _context.Database.BeginTransaction())
            {
                try
                {
                    Escritorio esc = _context.Escritorios.Find(id);
                    if (esc == null)
                    {
                        throw  new EscritorioException("Escritório em questão não existe mais no banco de dados.");
                    }

                    _context.Escritorios.Remove(esc);
                    _context.SaveChanges();
                    transacao.Commit();
                }
                catch (Exception e)
                {
                    transacao.Rollback();
                    throw new EscritorioException("Erro ao remover os dados de um escritório. Erro:" + e.HResult + " - " + e.Message );
                }
            }
        }
    }
}
