﻿using System;
using System.Runtime.Serialization;

namespace ADVCore.Models.Concret
{
    [Serializable]
    internal class CidadeException : Exception
    {
        public CidadeException()
        {
        }

        public CidadeException(string message) : base(message)
        {
        }

        public CidadeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CidadeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}