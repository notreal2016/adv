﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Context;
using ADVCore.Models.Abstract;
using ADVCore.Models.Context;

namespace ADVCore.Models.Concret
{
    /// <summary>
    /// Classe encarregada de persistir os dados de uma cidade no banco de dados
    /// </summary>
    public class CidadeRepository : ICidadeRepository
    {
        private AdvContext _context = FactoryContext.GetContext();

        /// <summary>
        /// Cria o obketo CidadeRepository
        /// </summary>
        /// <param name="context">Contexto de persistência</param>
        public CidadeRepository()
        {
            
        }

        /// <summary>
        /// Salva os dados de uma cidade no banco de dados
        /// </summary>
        /// <param name="cidade">Cidade cujos dados serão salvos</param>
        public void Save(Cidade cidade)
        {
            using (DbContextTransaction transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (cidade.Id == 0)
                    {
                        _context.Cidades.Add(cidade);
                    }
                    else
                    {
                        _context.Entry(cidade).State = EntityState.Modified;
                    }

                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw new CidadeException(e.Message);
                }
            }
        }

        /// <summary>
        /// Remove os dados de uma cidade do banco de dados
        /// </summary>
        /// <param name="idCidade">Id da cidade cujos dados serão removidos do banco de dados</param>
        public void Remove(long idCidade)
        {
            using (DbContextTransaction transaction = _context.Database.BeginTransaction())
            {
                Cidade cidade = _context.Cidades.Find(idCidade);
                try
                {
                    _context.Cidades.Remove(cidade);
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw new CidadeException(e.Message);
                }
            }
        }

        /// <summary>
        /// Retorna todas as cidades cadastradas em ordem alfabética
        /// </summary>
        /// <returns>Enumerable de cidades</returns>
        public IEnumerable<Cidade> FindAll()
        {
            return _context.Cidades.OrderBy(cidade => cidade.Nome);
        }
    }
}