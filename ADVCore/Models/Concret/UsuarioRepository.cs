﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Context;
using ADVCore.Models.Context;

namespace ADVCore.Models.Concret
{
    /// <summary>
    /// Classe encarregada na persistencia dos dados de um usuários.
    /// </summary>
    public class UsuarioRepository
    {
        private AdvContext _context = FactoryContext.GetContext();

        /// <summary>
        /// Persiste os dados de um usuário no banco de dados
        /// </summary>
        /// <param name="user">Usuario cujos dados serão persistidos</param>
        /// <returns>Usuário após a persistencia dos dados</returns>
        public Usuario Save(Usuario user)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (user.Id == 0)
                    {
                        _context.Usuarios.Add(user);
                    }
                    else
                    {
                        _context.Entry(user).State = EntityState.Modified;
                    }

                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e;
                }    
            }
            return user;
        }
        /// <summary>
        /// Exclui um usuário apartir do id do repassado como paramentro
        /// </summary>
        /// <param name="id">Id do usuário a ser removido</param>
        public void Remove(Int64 id)
        {
            using (var transacao = _context.Database.BeginTransaction())
            {
                try
                {
                    Usuario user = _context.Usuarios.Find(id);
                    _context.Usuarios.Remove(user);
                    _context.SaveChanges();
                    transacao.Commit();
                }
                catch (Exception e)
                {
                    transacao.Rollback();
                    throw e;
                }
            }
        }
        /// <summary>
        /// Busca por um usuário apartir do id repassado
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Usuário localizado</returns>
        public Usuario FindById(Int64 id)
        {
            return _context.Usuarios.Find(id);
        }
        /// <summary>
        /// Loga um usuário pelo login e senha
        /// </summary>
        /// <param name="login">Login do usuário</param>
        /// <param name="senha">Senha do usuário</param>
        /// <returns></returns>
        public Usuario Logar(String login, String senha)
        {
            var user = _context.Usuarios.Where(u => u.Login.Equals(login) && u.Senha.Equals(senha)).Single();
            if (user == null)
            {
                throw  new  Exception("Login ou senha inválidos");
            }

            return user;
        }
        /// <summary>
        /// Lista todos os usuários cadastrados
        /// </summary>
        /// <returns>Lista de usuários em ordem alfabética.</returns>
        public List<Usuario> All()
        {
            return _context.Usuarios.OrderBy(u => u.Nome).ToList();
        }

        /// <summary>
        /// Verifica se já existe um usuário cadastrado com o referido login
        /// </summary>
        /// <param name="login">Login a ser verificado</param>
        /// <returns>Booleano de confirmação</returns>
        public bool JaExisteLogin(String login)
        {
            return _context.Usuarios.Where(u => u.Login.ToUpper().Equals(login.ToUpper())).Count() != 0;

        }

        /// <summary>
        /// Procura por todos os usuário cadastrados que contenham o termo repassado como nome 
        /// </summary>
        /// <param name="nome">Nome a ser buscado</param>
        /// <returns>Lista de usuários em ordem alfabaticas localizados pelo termo</returns>
        public List<Usuario> FindByNome(String nome)
        {
            ;
            return _context.Usuarios.Where(u => u.Nome.ToLower().Contains(nome.ToLower())).OrderBy(u1 => u1.Nome)
                .ToList();
        }


    }
}
