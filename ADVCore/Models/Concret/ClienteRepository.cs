﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Context;
using ADVCore.Models.Abstract;
using ADVCore.Models.Context;
using ADVCore.Service;
using MySql.Data.MySqlClient;

namespace ADVCore.Models.Concret
{
    /// <summary>
    /// Classe encarregada de persistir os dados de um cliente no banco de dados.
    /// </summary>
    public class ClienteRepository : IClienteRepository
    {
        private AdvContext _context = FactoryContext.GetContext();

        /// <summary>
        /// Cria o objeto ClienteRepository 
        /// </summary>
        /// <param name="context">Contexto de origem</param>
        public ClienteRepository()
        {
            
        }

        public List<InformeCliente> FindClientes(long id = 0)
        {
            List<InformeCliente>lista = new List<InformeCliente>();
            String streng = _context.Database.Connection.ConnectionString;
            using (MySqlConnection conn = (MySqlConnection)_context.Database.Connection)
            {
                conn.Open();
                String SQL =
                    "select cliente.id, cliente.nome, cliente.cpf, cliente.Tipo, cliente.aniversario, cliente.processo, p1.cidade , p1.IdEscritorio " +
                    "from cliente left join (Select endereco.EnderecoId as EndId, cidade.nome as cidade, cidade.IdEscritorio " +
                    "from endereco left join cidade on cidade.id = endereco.cidade_id)as p1 on cliente.enderecoid = p1.endid;";
                MySqlCommand command = new MySqlCommand(SQL, conn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                 
                    lista.Add(new InformeCliente(dr.GetInt64("id"), dr.GetString("nome"), dr.GetString("cpf"),  dr.GetDateTime("aniversario"),
                        dr.GetString("processo"),
                       (dr["cidade"] == DBNull.Value)?"":dr.GetString("cidade"), (TipoCliente) int.Parse(dr["Tipo"].ToString()),
                        
                        dr["IdEscritorio"] == DBNull.Value?0: dr.GetInt64("IdEscritorio")));
                }
                dr.Close();
            }
            return lista.OrderBy(c => c.Nome).ToList();
        }
        /// <summary>
        /// Salva os dados de um cliente no contexto
        /// </summary>
        /// <param name="cliente">Cliente cujos dados serão salvos</param>
        public void Save(Cliente cliente)
        {
            using (DbContextTransaction transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (cliente.Id == 0)
                    {
                        _context.Clientes.Add(cliente);
                    }
                    else
                    {
                        _context.Entry(cliente).State = EntityState.Modified;
                    }

                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    if (e.HResult == -2146233087)
                    {
                        throw new ClienteException("Erro, já exite cliente cadastrado no sistema com o CPF : " +
                                                   cliente.Cpf + ".");
                    }

                    throw new ClienteException("Não foi possível gravar os dados do cliente," +
                                               " favor contatar suporte repassando o erro a seguir. Erro: "
                                               + e.HResult + " - " + e.Message);
                }
            }
        }

        /// <summary>
        /// Lista todos os clientes em ordem alfabetica
        /// </summary>
        /// <returns>Enumerable de clientes</returns>
        public IEnumerable<Cliente> FindAll()
        {
            return _context.Clientes.OrderBy(cliente => cliente.Nome);
        }

        /// <summary>
        /// Busca por um cliente apartir do id
        /// </summary>
        /// <param name="id">Id de busca</param>
        /// <returns>Cliente localizado</returns>
        public Cliente FindByID(long id)
        {
            Cliente c = _context.Clientes.Find(id);
            _context.Entry(c).Reload();
            return _context.Clientes.Find(id);
        }

        /// <summary>
        /// Busca um cliente apartir do CPF ou CNPJ dele
        /// </summary>
        /// <param name="cpf">CPF ou CNPJ</param>
        /// <returns>Cliente</returns>
        public Cliente FindByCPF(string cpf)
        {
            RefreshAll();
            DbSqlQuery<Cliente> result = _context.Clientes.SqlQuery("Select * from Cliente where cpf ='" + cpf + "';");

            return result == null ? null : result.First();

        }

        /// <summary>
        /// Busca por clientes que tenham em seu nome a informação repassada no parametro nome
        /// </summary>
        /// <param name="nome">Nome ou parte dele</param>
        /// <returns>Enumerable de clientes localizados</returns>
        public IEnumerable<Cliente> FindByName(string nome)
        {
            RefreshAll();
            return _context.Clientes.SqlQuery("Select * from Cliente where nome Like '%" + nome + "%';")
                .OrderBy(cliente => cliente.Nome);
        }

        /// <summary>
        /// Busca por um cliente a partir do processo repassado
        /// </summary>
        /// <param name="processo">Número do processo</param>
        /// <returns>Cliente localizado, caso contrario null</returns>
        public Cliente FindByProcesso(string processo)
        {
            RefreshAll();
            var clientes = _context.Clientes.SqlQuery("Select *  from cliente where processo = '" + processo + "';");
            return (clientes == null) ? null : clientes.First();
        }

        /// <summary>
        /// Busca todos os clientes de uma determinada cidade repassada como parametro
        /// </summary>
        /// <param name="cidade">Cidade repassada</param>
        /// <returns>Enumerable de clientes</returns>
        public IEnumerable<Cliente> FindByCidade(string cidade)
        {
            return _context.Clientes
                .SqlQuery(
                    "SELECT cliente.* FROM(advdata.cliente left join((endereco left join cidade on endereco.Cidade_Id = cidade.Id)) on cliente.EnderecoId = endereco.EnderecoId) where cidade.Nome Like '%" +
                    cidade + "%';").OrderBy(cliente => cliente.Nome);
        }

        /// <summary>
        /// Remove um cliente do contexto apartir do id repassado
        /// </summary>
        /// <param name="idCliente">Id do cliente</param>
        public void Remove(long idCliente)
        {
            using (DbContextTransaction transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Cliente cliente = this.FindByID(idCliente);
                    _context.Clientes.Remove(cliente);
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw new ClienteException("Erro ao tentar excluir o cliente, provavelmente há dados vinculados " +
                                               "que empede a sua exlcusão. Erro:" + e.Message);
                }
            }
        }

        /// <summary>
        /// Busca por todos os clientes aniversariantes do mês
        /// </summary>
        /// <param name="mes">Mês de busca</param>
        /// <returns>Enumerable de clientes</returns>
        public IEnumerable<InformeCliente> FindAniversariantes(long mes)
        {
            IEnumerable<InformeCliente> lista = FindClientes().Where(c => c.Aniversario.Month == mes).OrderBy(c1 => c1.Nome).ToList();
            return lista;
        }

        public Cliente RefreshCliente(Cliente cliente)
        {
            _context.Entry(cliente).Reload();
            cliente = _context.Clientes.Find(cliente.Id);
            return cliente; 
        }

        public void RefreshAll()
        {
            foreach (var entity in _context.ChangeTracker.Entries())
            {
                entity.Reload();
            }
        }
    }
}