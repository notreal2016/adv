﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Drawing;
using ADVCore.Service;
using Newtonsoft.Json;

namespace ADVCore.Models
{
    public enum TipoCliente
    {
        CLIENTE = 0, CONTATO = 1, DIVERSOS = 2
    }
    /// <summary>
    /// Classe encarregada de cuidar dos dados de cliente
    /// </summary>
    [Table("Cliente"),Serializable, JsonObject]
    public class Cliente
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;

        [Required(ErrorMessage = "Por favor inserir o nome do cliente.")]
        private String _nome;

        [Required(ErrorMessage = "Por favor inserir o CPF do cliente.")]
        private String _cpf;

        private byte[] _foto;

        [Required(ErrorMessage = "Por favor inserir a data de nascimento do cliente.")]
        private DateTime _aniversario;

        private String _relato;
        private String _processo;
        private String _email;
        private String _ponto;
        private Int64 _enderecoId;
        private String _apelido;
        private TipoCliente _tipo = TipoCliente.CLIENTE;
        public string Apelido
        {
            get { return _apelido; }
            set { _apelido = value; }
        }

        public TipoCliente Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        /// <summary>
        /// Cri a classe de cliente 
        /// </summary>
        public Cliente()
        {
            Telefones = new List<Telefone>();
            Historicos = new List<Historico>();
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }


        [Index("IX_Cpf", 2, IsUnique = true), MaxLength(14)]
        public string Cpf
        {
            get { return _cpf; }
            set { _cpf = value; }
        }


        public byte[] Foto
        {
            get { return _foto; }
            set { _foto = value; }
        }

        public DateTime Aniversario
        {
            get { return _aniversario; }
            set { _aniversario = value; }
        }

        public string Relato
        {
            get { return _relato; }
            set { _relato = value; }
        }

        public string Processo
        {
            get { return _processo; }
            set { _processo = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Ponto
        {
            get { return _ponto; }
            set { _ponto = value; }
        }

        //[ForeignKey("TelefoneId")]
        public virtual ICollection<Telefone> Telefones { get; set; }

        //[ForeignKey("HistoricoId")]

        public virtual ICollection<Historico> Historicos { get; set; }

        public long EnderecoId
        {
            get { return _enderecoId; }
            set { _enderecoId = value; }
        }

        [Required, ForeignKey("EnderecoId")] public virtual Endereco End { get; set; }

        public override string ToString()
        {
            return "Id: " + this.Id + " \t CPF: " + this._cpf + "\t Nascimento: " + this.Aniversario.ToShortDateString() + "\t Nome: " + this._nome.ToUpper() ;
        }

        [NotMapped]
        public String CidadeUF
        {
            get { return (End.Cidade !=null)? End.Cidade.Nome + "-" + End.Cidade.Uf:"\bCidade não cadastrada"; }
            private set { }
        }
    }
}