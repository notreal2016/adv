﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADVCore.Models
{
    [Table("Usuario")]
    public class Usuario
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;

        private String _login,_senha, _nome;
        private TipoUser _tipo;
        private long _idEscritorio;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public string Senha
        {
            get { return _senha; }
            set { _senha = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public TipoUser Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        public override string ToString()
        {
            return this.Nome.ToUpper();
        }

        public long IdEscritorio
        {
            get => _idEscritorio;
            set => _idEscritorio = value;
        }
    }

    public enum TipoUser
    {
        ADMINISTRADOR =0 , FUNCIONARIO =1
   
    }
}
