﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ADVCore.Models
{
    [Table("Escritorio"), Serializable, JsonObject]
    public class Escritorio
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private long _id;
        [Required]
        private string _nome;
        private string _telefone;
        private Endereco _endereco = new Endereco();

        public long Id
        {
            get => _id;
            set => _id = value;
        }

        public string Nome
        {
            get => _nome;
            set => _nome = value;
        }

        public string Telefone
        {
            get => _telefone;
            set => _telefone = value;
        }

        public Endereco Endereco
        {
            get => _endereco;
            set => _endereco = value;
        }

        public override string ToString()
        {
            return Nome.ToUpper();
        }
    }
}
