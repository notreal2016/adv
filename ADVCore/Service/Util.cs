﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADVCore.Service
{
    public class Util
    {
        public static Byte[] ImgToArray(Image image)
        {
            ImageConverter imgCon = new ImageConverter();
            return (byte[]) imgCon.ConvertTo(image, typeof(byte[]));
        }

        public static Image ArrayToImage(Byte[] array)
        {
            using (MemoryStream mStream = new MemoryStream(array))
            {
                return Image.FromStream(mStream);
            }
        }
    }
}