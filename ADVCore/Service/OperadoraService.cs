﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Context;
using ADVCore.Models;
using ADVCore.Models.Abstract;
using ADVCore.Models.Concret;

namespace ADVCore.Service
{
    public class OperadoraService
    {
        private IOperadoraRepository _repository;

        public OperadoraService(AdvContext context)
        {
            _repository = new OperadoraRepository();
        }

        public IEnumerable<Operadora> Operadoras()
        {
            return _repository.FindAll();
        }

        public void Save(Operadora operadora)
        {
            _repository.Save(operadora);
        }

        public void Remove(Operadora operadora)
        {
            _repository.Remove(operadora.Id);
        }
    }
}