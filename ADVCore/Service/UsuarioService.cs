﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Models.Concret;
using ADVCore.Models;

namespace ADVCore.Service
{
    public class UsuarioService
    {
        private UsuarioRepository _ur;
        
        public UsuarioService(UsuarioRepository ur)
        {
            _ur = ur;
        }
        /// <summary>
        /// Salva os dados de um usuário 
        /// </summary>
        /// <param name="user">Usuário a ser salvo</param>
        /// <returns>Usuário após dados serem salvos</returns>
        public Usuario Save(Usuario user)
        {
            try
            {
                ValidaUsuario(user);
                return _ur.Save(user);
            }
            catch (Exception e)
            {
                throw e;
            }
            
        }
        /// <summary>
        /// Valida os dados de um usuário 
        /// </summary>
        /// <param name="user">Usuário a ser validado</param>
        private void ValidaUsuario(Usuario user)
        {
            if (ValidaDados.ExistCaracterEspeciais(user.Nome.Trim()) || user.Nome.Trim().Equals(""))
            {
                throw  new Exception("501-Nome do usuário inválido.");
            }

            if (ValidaDados.ExistCaracterEspeciais(user.Login) || user.Login.Trim().Equals(""))
            {
                throw  new Exception("502-Login do usuário inválido.");
            }

            if (user.Login.Trim().Equals(""))
            {
                throw  new Exception("503-Senha do usuário inválido.");
            }

            if (_ur.JaExisteLogin(user.Login) && user.Id == 0)
            {
                throw  new Exception("502-Já existe outro usuário com o mesmo login.");
            }
        }
        /// <summary>
        /// Remove um usuário de um banco de dados a partir do id repassado
        /// </summary>
        /// <param name="id">Id do usuário</param>
        public void Remove(Int64 id)
        {
            _ur.Remove(id);
        }
        /// <summary>
        /// Busca por um usuário a partir do id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Usuario Find(Int64 id)
        {
            return _ur.FindById(id);
        }
        /// <summary>
        /// Lista todo os usuários em ordem alfabetica
        /// </summary>
        /// <returns></returns>
        public List<Usuario> AllUser()
        {
            return _ur.All();
        }
        /// <summary>
        /// Loga o usuário no sistema 
        /// </summary>
        /// <param name="login">Login do usuário</param>
        /// <param name="senha">Senha do usuário</param>
        /// <returns>Usuário logado</returns>
        public Usuario Logar(string login, string senha)
        {
            

            if (ValidaDados.ExistCaracterEspeciais(login) || login.Trim().Equals(""))
            {
                throw  new Exception("503-Login do usuário inválido.");
            }

            try
            {
                return _ur.Logar(login, senha);
            }
            catch (Exception e)
            {
                throw new Exception("503-" +e.Message);
            }
            
        }

        /// <summary>
        /// Busca por usuários que contenham em seu nome o termo repassado
        /// </summary>
        /// <param name="nome">Nome do usuário</param>
        /// <returns>Lista de usuários localizados</returns>
        public List<Usuario> FindByNome(String nome)
        {
            if (nome.Trim().Equals("") || ValidaDados.ExistCaracterEspeciais(nome))
            {
                throw  new Exception("Informações incorretas para busca pelo nome do usuário.");
            }

            return _ur.FindByNome(nome);

        }

    }
}
