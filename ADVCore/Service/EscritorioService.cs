﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Models;
using ADVCore.Models.Concret;
using ADVCore.Models.Context;

namespace ADVCore.Service
{
    public class EscritorioService
    {
        private IEscritorioRepository _er;

        public EscritorioService()
        {
            _er = new EscritorioRepository(FactoryContext.GetContext());
        }

        public Escritorio Salva(Escritorio escritorio)
        {
           return _er.Salva(escritorio);
        }

        public void Excluir(long id)
        {
            _er.Remove(id);
        }

        public List<Escritorio> FindAll()
        {
            return _er.FindAll();
        }

        public Escritorio FindByid(long id)
        {
            return _er.FindById(id);
        }

      
    }
}
