﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ADVCore.Context;
using ADVCore.Models;
using ADVCore.Models.Concret;
using Newtonsoft.Json.Linq;

namespace ADVCore.Service
{
    public class ClienteService
    {
        
        private ClienteRepository repositorio;
        private CidadeRepository repoCidade;
        private OperadoraRepository repoOperadora;

        public ClienteService()
        {
            
            repositorio = new ClienteRepository();
            repoCidade = new CidadeRepository();
            repoOperadora = new OperadoraRepository();
        }

        /// <summary>
        /// Metódo retorna todos os clientes cadastrados.
        /// </summary>
        /// <returns></returns>
        public List<Cliente> Clientes()
        {
            return repositorio.FindAll().ToList();
        }

        public Cliente FindById(Int64 id)
        {
            return repositorio.FindByID(id);
        }
        public List<InformeCliente> FindClientes( long id = 0)
        {
            return repositorio.FindClientes(id);
        }
        /// <summary>
        /// Busca por clientes apartir de um nome ou parte dele.
        /// </summary>
        /// <param name="nome">Termo de busca</param>
        /// <returns>Lista com o nome dos clientes</returns>
        public List<Cliente> BuscaPorNome(String nome)
        {
            if (nome.Trim().Equals(""))
            {
                throw new ClienteException("Não foi repassado nenhum nome para busca.");
            }

            return repositorio.FindByName(nome).OrderBy(cliente => cliente.Nome).ToList();
        }

        /// <summary>
        /// Busca por um cliente apartir do CPF repassado sendo só aceito números.
        /// </summary>
        /// <param name="CPF">CPF que será procurado na base</param>
        /// <returns>Cliente localizado</returns>
        public Cliente BuscaPorCPF(String CPF)
        {
            if (!ValidaDados.SoNumeros(CPF))
            {
                throw new ClienteException("Para busca deve ser repassado somente números.");
            }

            return repositorio.FindByCPF(CPF);
        }

        /// <summary>
        /// Busca cliente pelo número do processo repassado
        /// </summary>
        /// <param name="processo">Número do processo</param>
        /// <returns>Cliente localizado pelo processo</returns>
        public Cliente BuscaPorProcesso(String processo)
        {
            if (processo.Trim().Equals(""))
            {
                throw new ClienteException("Deve ser repassado um número de processo para busca");
            }

            return repositorio.FindByProcesso(processo);
        }

        /// <summary>
        /// Método efetua busca por meio de nome de uma cidade repassado por termo 
        /// </summary>
        /// <param name="termo">Nome da cidade</param>
        /// <returns>Lista de cidades</returns>
        public List<Cliente> BuscaPorCidade(string termo)
        {
            return repositorio.FindByCidade(termo).OrderBy(cliente => cliente.Nome).ToList();
        }

        public List<Cidade> CidadesAll()
        {
            return repoCidade.FindAll().ToList();
        }

        public void Save(Cliente cliente)
        {
            //Regra de validação de cliente
            try
            {
                repositorio.Save(cliente);
            }
            catch (Exception e)
            {
                throw new ClienteException("Erro ao gravar dados de cliente:" + e.HResult + " " + e.Message);
            }
        }

        public List<Operadora> OperadprasAll()
        {
            return repoOperadora.FindAll().ToList();
        }

        public Dictionary<string, object> FichaCliente(Cliente cliente)
        {
            return new Dictionary<string, object>()
            {
                {"Ficha_Cliente", new List<Cliente>() {cliente}},
                {"Telefone", cliente.Telefones},
                {"Historico", cliente.Historicos},
                {"Endereco", new List<Endereco>() {cliente.End}},
                {"Cidade", new List<Cidade>() {cliente.End.Cidade}}
            };
        }


        public Dictionary<string, object> RelatorioFiltro(List<Cliente> clientes)
        {
            return new Dictionary<string, object>()
            {
                {"Clientes", clientes}
            };
        }

        public Dictionary<string, object> Colaboradores(List<InformeCliente> clientes)
        {
            return new Dictionary<string, object>()
            {
                {"Clientes", clientes.Where(c => c.Tipo == TipoCliente.CONTATO).ToList()}
            };
            
        }
        public Dictionary<string, object> RelatorioFiltro(List<InformeCliente> clientes)
        {
            return new Dictionary<string, object>()
            {
                {"Clientes", clientes}
            };
        }

        public Dictionary<string, object> Aniversariantes(Int64 mes)
        {
            return new Dictionary<string, object>()
            {
                {"Clientes", repositorio.FindAniversariantes(mes).ToList()}
            };
        }

        public void Excluir(Cliente cliente)
        {
            repositorio.Remove(cliente.Id);
        }

        public Cliente RefreshCliente(Cliente cliente)
        {
            return repositorio.RefreshCliente(cliente);
        }

        public void RefreshAll()
        {
            repositorio.RefreshAll();
        }

        public void Remove(Int64 id)
        {
            repositorio.Remove(id);
        }
    }

    public class InformeCliente
    {
        private Int64 _id;
        private String _nome;
        private String _CPF;
        private DateTime _aniversario;
        private String _processo;
        private String _cidade;
        private TipoCliente _tipo;
        private long _idEscritorio;
        public InformeCliente(long id, string nome, string cpf, DateTime aniversario, string processo, string cidade, TipoCliente tipo, long idEscritorio = 0)
        {
            _id = id;
            _nome = nome;
            _CPF = cpf;
            _aniversario = aniversario;
            _processo = processo;
            _cidade = cidade;
            _tipo = tipo;
            _idEscritorio = idEscritorio;
        }

        public long IdEscritorio => _idEscritorio;

        public TipoCliente Tipo => _tipo;

        public string Processo => _processo;

        public string Cidade => _cidade;

        public long Id => _id;

        public string Nome => _nome;

        public string Cpf => _CPF;

        public DateTime Aniversario => _aniversario;

        public override string ToString()
        {
            return "Id: " + this.Id + " \t CPF: " + this._CPF + "\t Nascimento: " + this.Aniversario.ToShortDateString() + "\t Nome: " + this._nome.ToUpper() ;
        }

    }
}