﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Context;
using ADVCore.Models;
using ADVCore.Models.Abstract;
using ADVCore.Models.Concret;
using ADVCore.Models.Context;

namespace ADVCore.Service
{
    public class CidadeService
    {
        
        private ICidadeRepository repoCidade;
        private IEscritorioRepository _er;

        public CidadeService()
        {
            _er = new EscritorioRepository(FactoryContext.GetContext());
            repoCidade = new CidadeRepository();
        }

        public IEnumerable<Cidade> Cidades()
        {
            return repoCidade.FindAll();
        }

        public void Save(Cidade cidade)
        {
            //Regra de validação de cliente
            try
            {
                repoCidade.Save(cidade);
            }
            catch (Exception e)
            {
                throw new ClienteException("Erro ao gravar dados da cidade:" + e.HResult + " " + e.Message);
            }
        }

        public List<Escritorio> Escritorios()
        {
            return _er.FindAll();
        }
    }
}