﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Context;
using ADVCore.Models;
using ADVCore.Models.Abstract;
using ADVCore.Models.Concret;
using ADVCore.Models.Context;
using ADVCore.Service;
using MySql.Data.MySqlClient;

namespace ADVCore
{
    class Program
    {
        static void Main(string[] args)
        {
            AdvContext _context = FactoryContext.GetContext();

            var usuarios = _context.Usuarios.Where(u => u.Nome.Contains("Laerton")).OrderBy(u1 => u1.Nome).ToList();
             
            usuarios.ForEach(u => Console.WriteLine(u.Nome));
            Console.ReadLine();

        }
    }
}