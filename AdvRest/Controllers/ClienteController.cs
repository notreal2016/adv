﻿using ADVCore.Models;
using ADVCore.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace AdvRest.Controllers
{
    [RoutePrefix("api/Cliente")]
    public class ClienteController : ApiController
    {
        private ClienteService _service = new ClienteService();

        [AcceptVerbs("POST")]
        [Route("Salvar")]
        public IHttpActionResult Save(Cliente cliente)
        {
            try
            {
                _service.Save(cliente);
                return Ok("Salvo com sucesso!");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [AcceptVerbs("DELETE")]
        [Route("RemoveCliente/{id}")]
        public String Remove(Int64 id)
        {
            try
            {
                _service.Remove(id);
                return "Cliente removido com sucesso!";
            }
            catch (Exception e)
            {
                return "Erro ao remover o cliente. " + e.Message;
            }
        }

        [AcceptVerbs("GET")]
        [Route("ListaClientes")]
        public List<InformeCliente> ListaClientes()
        {
            return _service.FindClientes();
        }
    }
}
